<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AdegacSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $meetings = ['Assemble Generale', 'Bureau Executif'];
        foreach ($meetings as $meeting){
            $meet = new \App\Meeting();
            $meet->display_name = $meeting;
            $meet->name = Str::slug($meeting);
            $meet->description = $meeting.', description';
            $meet->save();
        }

        $bureaus = ['Influence 2017', 'Influence 2018'];
        foreach ($bureaus as $bureau){
            $bureauVar = new \App\Team();
            $bureauVar->display_name = $bureau;
            $bureauVar->name = Str::slug($bureau);
            $bureauVar->description = $bureau.', description';
            $bureauVar->start_date = \Carbon\Carbon::now()->subMonths(3);
            $bureauVar->end_date = \Carbon\Carbon::now()->subMonths(3)->addMonths(12);
            $bureauVar->save();
        }

        factory(\App\Member::class, 10)->create();
    }
}
