<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(\App\Member::class, function (Faker $faker) {
    return [
        'last_name' => $faker->lastName,
        'first_name' => $faker->firstName,
        'dob' => $faker->dateTime,
        'pob' => $faker->word,
        'gender' => $faker->randomElement(['Male', 'Female']),
        'passport_number' => '18GA'.$faker->randomNumber(5),
        'occupation_title' => $faker->randomElement(['Student', 'Worker']),
        'occupation_type' => $faker->word,
        'occupation_place' => $faker->word,
        'cell_number' => $faker->randomNumber(),
        'address' => $faker->words(15, true),
        'email' => $faker->unique()->email,
        'joining_date' => $faker->dateTime,
        'nk_last_name' => $faker->unique()->lastName,
        'nk_first_name' => $faker->unique()->firstName,
        'nk_contact' => $faker->randomNumber(),
    ];
});
