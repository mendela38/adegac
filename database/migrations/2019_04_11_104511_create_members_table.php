<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('last_name');
            $table->string('first_name');
            $table->date('dob');
            $table->string('pob');
            $table->enum('gender', ['Male', 'Female']);
            $table->string('passport_number', 11)->unique();
            $table->enum('occupation_title', ['Student', 'Worker']);
            $table->string('occupation_type')->nullable();
            $table->string('occupation_place')->nullable();
            $table->string('cell_number', 12)->unique();
            $table->text('address');
            $table->string('email', 50)->unique();
            $table->string('picture')->nullable();
            $table->date('joining_date')->nullable();
            $table->string('nk_last_name')->nullable();
            $table->string('nk_first_name')->nullable();
            $table->string('nk_contact')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
