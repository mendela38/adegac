@extends('layouts.master')

@section('title')
    Add Meeting
@endsection

@section('content')
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <h3 class="page-header text-center">Meeting Information</h3>
                            @include('partials.error')

                            @include('partials.formMessage')


                            <form class="form-horizontal" role="form" method="POST"
                                  action="{{ route('meetings.store') }}">

                                {{csrf_field()}}

                                <div class="row">
                                    <div class="col-lg-offset-2 col-lg-8 col-xs-12">

                                        <div class="form-group{{$errors->has('name') ? ' has-error' : ' has-warning'}}">
                                            <label for="">Meeting Name</label>
                                            <input type="text" class="form-control" name="name"
                                                   placeholder="Meeting Name" autocomplete="off"
                                                   value="{{old('name')}}">
                                        </div>

                                        <div class="form-group{{$errors->has('description') ? ' has-error' : ''}}">
                                            <label for="">Description</label>
                                            <textarea name="description" id="description"
                                                      placeholder="Description" class="form-control"
                                                      rows="3">{{ old('description') }}</textarea>
                                        </div>

                                        <div class="form-group">
                                            <div class=" text-right">
                                                <button type="submit" class="btn btn-dark "><i
                                                            class="fa fa-plus-circle"></i> Add
                                                </button>
                                                <a href="{{ route('meetings.index') }}" class="btn btn-default"><i
                                                            class="fa fa-reply"></i> Back</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
