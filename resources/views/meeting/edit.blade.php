@extends('layouts.master')

@section('title')
    Edit Meeting
@endsection

@section('pageScript')
    <link href="{{asset('/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection


@section('content')
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            @include('partials.error')

                            @include('partials.formMessage')

                            <h3 class="page-header text-center">Meeting Information</h3>

                            <form class="form-horizontal" role="form" method="POST"
                                  action="{{ route('meetings.update', ['meeting' => $meeting]) }}">

                                {{csrf_field()}}
                                {{ method_field('patch') }}
                                <input type="hidden" name="id" value="{{ $meeting->id }}">

                                <div class="row">
                                    <div class="col-lg-offset-2 col-lg-8 col-xs-12">

                                        <div class="form-group{{$errors->has('name') ? ' has-error' : ' has-warning'}}">
                                            <label for="">Meeting Name</label>
                                            <input type="text" class="form-control" name="name"
                                                   placeholder="Meeting Name" autocomplete="off"
                                                   value="{{ old('name') ? old('name') : $meeting->display_name}}">
                                        </div>

                                        <div class="form-group{{$errors->has('description') ? ' has-error' : ''}}">
                                            <label for="">Meeting Name</label>
                                            <textarea name="description" id="description" class="form-control"
                                                      rows="3">{{ old('description') ? old('description') : $meeting->description }}</textarea>
                                        </div>

                                        <div class="form-group">
                                            <div class=" text-right">
                                                <button type="submit" class="btn btn-dark "><i class="fa fa-save"></i> Save
                                                </button>
                                                <a href="{{ route('meetings.index') }}" class="btn btn-default"><i class="fa fa-reply"></i> Back</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')
    <script src="{{asset('/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('/js/adegac.js')}}"></script>
    <script>
        datePicker();
    </script>
@endsection
