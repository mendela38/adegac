@extends('layouts.master')

@section('title')
    Meetings
@endsection

@section('pageScript')
    <link href="{{asset('/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>

@endsection


@section('content')
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group text-right">
                        <a href="{{route('meetings.create')}}" class="btn btn-md btn-dark waves-light waves-effect"><i
                                    class="fa fa-plus-circle" title="Add Meeting"></i> Add Meeting
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            @include('partials.error')

                            <table id="meeting_table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Created at</th>
                                    <th>Updated</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($meetings as $index => $meeting)
                                    <tr>
                                        <td>{{ $index+1 }}</td>
                                        <td>{{$meeting->display_name}}</td>
                                        <td>{{ \Carbon\Carbon::parse($meeting->created_at)->toDayDateTimeString() }}</td>
                                        <td>{{ \Carbon\Carbon::parse($meeting->updated_at)->toDayDateTimeString() }}</td>
                                        <td>

                                            <a href="{{ route('meetings.edit', ['meeting' => $meeting]) }}" class="btn btn-xs btn-success waves-effect"><i
                                                        class="fa fa-pencil"></i>
                                            </a>

                                            <a href="{{ route('meetings.destroy', ['meeting' => $meeting]) }}" onclick="deleting('#delete-meeting{{$meeting->id}}', 'Meeting')"
                                               class="btn btn-xs btn-danger waves-effect"><i
                                                        class="fa fa-trash"></i>
                                            </a>
                                            <form class="hidden" action="{{ route('meetings.destroy', ['meeting' => $meeting]) }}" method="POST" id="delete-meeting{{$meeting->id}}">
                                                {{ @csrf_field() }}
                                                {{ method_field("DELETE") }}
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/jszip.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/pdfmake.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/vfs_fonts.js')}}"></script>
    <script src="{{asset('/plugins/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/responsive.bootstrap.min.js')}}"></script>

    <script>
        $(document).ready(function () {

            var $memberTable = $('#meeting_table');


            var table = $memberTable.DataTable({
                stateSave: true,
                "ordering": true,
                language: {
                    searchPlaceholder: "Searching...."
                },
                dom: 'frtp',
                responsive: true
            });
        });
    </script>
@endsection
