@extends('layouts.master')

@section('title')
    Member's Finance
@endsection

@section('pageScript')
    <link href="{{asset('/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet" type="text/css">

@endsection

@section('content')

    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <h3 class="page-header text-center">Financial History
                                    <a href="{{ route('contributions.index') }}" class="btn btn-default pull-right"><i class="fa fa-reply"></i> Back</a></h3>
                                @include('partials.error')
                                @permission('add-contributions')
                                <form class="form-horizontal" role="form" method="POST"
                                      action="{{ route('contributions.store') }}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="member_id" value="{{ $member->id }}">
                                    <div class="row" style="margin-bottom: 15px;">
                                        <div class="col-lg-offset-1 col-lg-4 col-xs-12">
                                            <label for="">Payment Date</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control date" name="payment_date"
                                                       autocomplete="off" title=""
                                                       value="" placeholder="Select Date" required readonly>
                                                <span class="input-group-addon bg-custom b-0">
                                                        <i class="mdi mdi-calendar"></i>
                                                    </span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-xs-12">
                                            <label for="">Amount Paid</label>
                                            <input type="number" class="form-control" name="amount"
                                                   autocomplete="off" title=""
                                                   placeholder="Amount Paid" value="" required>
                                        </div>
                                        <div class="col-lg-2 col-xs-12" style="margin-top: 30px">
                                            <button type="submit" class="btn btn-sm btn-dark btn-block"><i
                                                    class="fa fa-plus-circle"></i> Add
                                            </button>
                                        </div>

                                    </div>
                                </form>
                                @endpermission
                                <div class="row">
                                    <div class="col-lg-offset-1 col-lg-10 col-xs-12">
                                        <table id="financial_history_table" class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                                <th>Amount Paid</th>
                                                @permission('delete-contributions')
                                                <th style="width: 15px"></th>
                                                @endpermission
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($member->financialHistory as $index => $contribution)
                                                <tr>
                                                    <td>{{$index+1}}</td>
                                                    <td>{{$contribution->payment_date}}</td>
                                                    <td class="text-right">
                                                        R {{number_format($contribution->amount, 2)}}</td>
                                                    @permission('delete-contributions')
                                                    <td>
                                                        <a href="{{ route('contributions.destroy', ['contribution' => $contribution]) }}"
                                                           onclick="deleting('#delete-contribution{{$contribution->id}}', 'Contribution')"
                                                           class="btn btn-xs btn-block btn-danger waves-effect"><i
                                                                class="fa fa-trash" title="Delete Member"></i>
                                                        </a>
                                                        <form class="hidden"
                                                              action="{{ route('contributions.destroy', ['contribution' => $contribution]) }}"
                                                              method="POST"
                                                              id="delete-contribution{{$contribution->id}}">
                                                            {{ @csrf_field() }}
                                                            {{ method_field("DELETE") }}
                                                        </form>
                                                    </td>
                                                    @endpermission
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')
    <script src="{{asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/responsive.bootstrap.min.js')}}"></script>
    <script src="{{asset('/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        datePicker();
        $(document).ready(function () {
            $('#financial_history_table').DataTable({
                stateSave: true,
                "ordering": false,
                language: {
                    searchPlaceholder: "Searching...."
                },
                dom: 'frtp',
                responsive: true
            });
        });
    </script>
@endsection
