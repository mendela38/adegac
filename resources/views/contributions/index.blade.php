@extends('layouts.master')

@section('title')
    Members Contributions
@endsection

@section('pageScript')
    <link href="{{asset('/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection


@section('content')

    <div class="page-content-wrapper ">
        <div class="container">
            @permission('browse-users')
            <div class="form-group text-right">
                <a href="{{route('people.index')}}" class="btn btn-md btn-dark waves-effect"><i
                            class="fa fa-reply"></i> Back to Members</a>
            </div>
            @endpermission
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            @include('partials.error')

                            <table id="contribution_table" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <th>Passport No</th>
                                    <th>Contact No</th>
                                    <th>Email</th>
                                    <th>Total Amount</th>
                                    <th>Member status</th>
                                    <th>Date</th>
                                    @permission('edit-contributions')
                                    <th></th>
                                    @endpermission
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($members as $member)
                                    <tr>
                                        <td>{{$member->last_name}}, {{$member->first_name}}</td>
                                        <td>{{$member->passport_number}}</td>
                                        <td>{{$member->cell_number}}</td>
                                        <td>{{$member->email}}</td>
                                        <td>{{$member->amount === null ? 'No Contribution' : 'R '. number_format($member->amount, 2)}}</td>
                                        <td>{{$member->title === null ? 'Community Member' : 'Bureau Member'}}</td>
                                        <td>{{$member->payment_date === null ? ' - ': $member->payment_date}}</td>
                                        @permission('edit-contributions')
                                        <td>
                                            <a href="{{ route('contributions.edit', ['contribution' => $member]) }}"
                                            class="btn btn-info btn-xs"><i class="fa fa-edit"></i> View</a>
                                        </td>
                                        @endpermission
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/responsive.bootstrap.min.js')}}"></script>
    <script>
        $(document).ready(function () {

            $('#contribution_table').DataTable({
                stateSave: true,
                "ordering": false,
                language: {
                    searchPlaceholder: "Searching...."
                },
                dom: 'frtp',
                responsive: true
            });
        });
    </script>
@endsection
