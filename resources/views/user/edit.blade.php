@extends('layouts.master')

@section('title')
    Edit User
@endsection

@section('pageScript')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="page-content-wrapper ">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <h3 class="page-header text-center">User Information</h3>
                        @include('partials.error')
                        <div class="row">
                            <div class="col-md-offset-2 col-md-8">
                                <form method="POST" action="{{ route('users.update', $user) }}">
                                    @csrf
                                    {{ method_field("PATCH") }}
                                    <div class="modal-body">
                                        <div class="container">
                                            <div class="row form-group">
                                                <label for="member"
                                                       class="col-md-12 col-form-label text-md-right">Member
                                                    Full name</label>
                                                <div class="col-md-12">
                                                    <select name="member" id="member"
                                                            class="form-control select">
                                                        <option value="reset">-- Unassigned
                                                            Member --
                                                        </option>
                                                        @foreach($members as $member)
                                                            <option value="{{ $member->id }}"
                                                                {{ old('member') == $member->id ? 'selected' : $user->member()->first() == null ? '' : $user->member()->first()->id == $member->id ? 'selected' : '' }}>
                                                                {{ $member->last_name . ' ' . $member->first_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="department"
                                                       class="col-md-12 col-form-label text-md-right">Department</label>

                                                <div class="col-md-12">
                                                    <select name="department" id="department"
                                                            class="form-control select">
                                                        <option value="">-- Select Department
                                                            --
                                                        </option>
                                                        @foreach($departments as $department)
                                                            <option
                                                                value="{{ $department->id }}" {{ old('department') == $department->id ? 'selected' : $user->department() == $department->id ? 'selected' :'' }}>
                                                                {{ $department->display_name}}</option>
                                                        @endforeach
                                                    </select>

                                                    @if ($errors->has('department'))
                                                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('department') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="bureau"
                                                       class="col-md-12 col-form-label text-md-right">Bureau</label>
                                                <div class="col-md-12">
                                                    <select name="bureau" id="bureau"
                                                            class="form-control select">
                                                        <option value="">-- Select a bureau --
                                                        </option>
                                                        @foreach($bureaus as $bureau)
                                                            <option
                                                                value="{{ $bureau->id }}" {{ old('bureau') == $bureau->id ? 'selected' : $user->bureau() == $bureau->id ? 'selected' : '' }}>{{ $bureau->display_name}}</option>
                                                        @endforeach
                                                    </select>

                                                    @if ($errors->has('bureau'))
                                                        <span class="invalid-feedback"
                                                              role="alert">
                                                                                    <strong>{{ $errors->first('bureau') }}</strong>
                                                                                </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="state"
                                                       class="col-md-12 col-form-label text-md-right">State</label>
                                                <div class="col-md-12">
                                                    <select name="state" id="state"
                                                            class="form-control select">
                                                        <option value="0" {{ $user->state == 0 ? 'selected':'' }}>Disable</option>
                                                        <option value="1" {{ $user->state == 1 ? 'selected': '' }}>Active</option>
                                                    </select>

                                                    @if ($errors->has('bureau'))
                                                        <span class="invalid-feedback"
                                                              role="alert">
                                                                                    <strong>{{ $errors->first('bureau') }}</strong>
                                                                                </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-dark">
                                            <i class="fa fa-plus-circle"></i> Assign</button>
                                        <a href="{{ route('users.index') }}" class="btn btn-default"><i
                                                class="fa fa-reply"></i> Back</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.select').select2();
        });
    </script>
@endsection
