@extends('layouts.master')

@section('title')
    Add User
@endsection

@section('pageScript')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
@endsection

@section('content')
    <div class="page-content-wrapper ">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <h3 class="page-header text-center">User Information</h3>
                        @include('partials.error')
                        <div class="container">
                            <form method="POST" action="{{ route('users.store') }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="member"
                                           class="col-md-4 col-form-label text-right">Member Full name</label>

                                    <div class="col-md-6">
                                        <select name="member" id="member" class="form-control select">
                                            <option value="">-- Select Member --</option>
                                            @foreach($members as $member)
                                                <option
                                                    value="{{ $member->id }}" {{ old('member') == $member->id ? 'selected' : '' }}>{{ $member->last_name . ' ' . $member->first_name}}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="department"
                                           class="col-md-4 col-form-label text-right">Department</label>

                                    <div class="col-md-6">
                                        <select name="department" id="department" class="form-control select">
                                            <option value="">-- Select Department --</option>
                                            @foreach($departments as $department)
                                                <option
                                                    value="{{ $department->id }}" {{ old('department') == $department->id ? 'selected' : '' }}>{{ $department->display_name}}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('department'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('department') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="bureau"
                                           class="col-md-4 col-form-label text-right">Bureau</label>

                                    <div class="col-md-6">
                                        <select name="bureau" id="bureau" class="form-control select">
                                            <option value="">-- Select a bureau --</option>
                                            @foreach($bureaus as $bureau)
                                                <option
                                                    value="{{ $bureau->id }}" {{ old('bureau') == $bureau->id ? 'selected' : '' }}>{{ $bureau->display_name}}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('bureau'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bureau') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="state"
                                           class="col-md-4 col-form-label text-right">State</label>

                                    <div class="col-md-6">
                                        <select name="state" id="state" class="form-control select">
                                            <option
                                                value="1" {{ old('state') == 1 ? 'selected' : '' }}>Active
                                            </option>
                                            <option
                                                value="0" {{ old('state') == 0 ? 'selected' : '' }}>Disable
                                            </option>


                                        </select>

                                        @if ($errors->has('bureau'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bureau') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password"
                                           class="col-md-4 col-form-label text-right">{{ __('Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password"
                                               class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                               name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password-confirm"
                                           class="col-md-4 col-form-label text-right">{{ __('Confirm Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control"
                                               name="password_confirmation" required>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-2 col-md-offset-8">
                                        <div class="pull-right">
                                            @permission('add-users')
                                            <button type="submit" class="btn btn-dark"><i class="fa fa-plus-circle"></i>
                                                Add
                                            </button>
                                            @endpermission
                                            <a href="{{ route('users.index') }}" class="btn btn-default"><i
                                                    class="fa fa-reply"></i> Back</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.select').select2();
        });
    </script>
@endsection
