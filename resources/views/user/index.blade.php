@extends('layouts.master')

@section('title')
    Users
@endsection

@section('pageScript')
    <link href="{{asset('/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection


@section('content')

    <div class="page-content-wrapper ">
        <div class="container">
            @permission('add-users')
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group text-right">
                        <a href="{{route('users.create')}}"
                           class="btn btn-md btn-dark waves-light waves-effect"><i
                                class="fa fa-plus-circle" title="Add Member"></i> Add User
                        </a>
                    </div>
                </div>
            </div>
            @endpermission
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            @include('partials.error')
                            <table id="bureau_table" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th style="width: 5%">#</th>
                                    <th style="width: 15%">Department</th>
                                    <th style="width: 20%">Email</th>
                                    <th style="width: 20%">Member</th>
                                    <th style="width: 15%">Bureau</th>
                                    <th style="width: 10%">Verified</th>
                                    @permission('edit-users|delete-users')
                                    <th style="width: 5%">Actions</th>
                                    @endpermission
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $index => $user)
                                    <tr>
                                        <td>{{$index+1}}</td>
                                        <td>{{ $user->roles()->first() !== null ? $user->roles()->first()->display_name : "No Department assigned." }}</td>
                                        <td style="width: 20px">{{$user->email}}</td>
                                        <td>{{ $user->fullName('No Name') }}</td>
                                        <td>{{ $user->rolesTeams()->first() !== null ? $user->rolesTeams()->first()->display_name : "No Bureau assigned."}}</td>
                                        <td>{{ $user->email_verified_at === null ? 'Not Verified Yet' : \Carbon\Carbon::parse($user->email_verified_at)->diffForHumans()  }} {{ $user->state == 1 ? ' - Active' : '' }}</td>
                                        @permission('edit-users|delete-users')
                                        <td>
                                            @permission('edit-users')
                                            <!-- Button trigger modal -->
                                            <a href="{{ route('users.edit', $user) }}" class="btn btn-success btn-xs">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            @endpermission
                                            @permission('delete-users')
                                            <a href="{{ route('users.destroy', ['user' => $user]) }}" type="button"
                                               onclick="deleting('#delete-user{{$user->id}}', 'User')"
                                               class="btn btn-xs btn-danger waves-effect"><i
                                                    class="fa fa-trash"></i>
                                            </a>

                                            <form class="hidden"
                                                  action="{{ route('users.destroy', ['user' => $user]) }}" method="POST"
                                                  id="delete-user{{$user->id}}">
                                                {{ @csrf_field() }}
                                                {{ method_field("DELETE") }}
                                            </form>
                                            @endpermission
                                        </td>
                                        @endpermission
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/responsive.bootstrap.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#bureau_table').DataTable({
                stateSave: true,
                "ordering": true,
                language: {
                    searchPlaceholder: "Searching...."
                },
                dom: 'frtp',
                responsive: true
            });
        });
    </script>
@endsection
