@if(!$errors->any())
    <div class="col-lg-12 alert alert-warning text-center display">
        <h4>Fields in orange must not be empty!!!</h4>
    </div>
    <script>
        window.setTimeout(function () {
            $(".display").fadeTo(700, 0).slideUp(500, function () {
                $(this).remove();
            });
        }, 4000);
    </script>
@endif