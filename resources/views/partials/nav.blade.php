<div class="topbar-left visible-lg" style="background-color: #c7c7c7">
    <div class="text-center">
        <a href="{{ route('people.index') }}" class="logo">
            <span style="color: #0cb800">AD</span><span style="color: #f2f000">EG</span><span style="color: blue">AC</span>
        </a>
        <a href="index.html" class="logo-sm">
            <small style="color: #0cb800">AD</small><small style="color: #f2f000">EG</small><small style="color: blue">AC</small>
        </a>
    </div>
</div>
<div class="navbar navbar-default" role="navigation">
    <div class="container" style="padding: 0">
        <div class="">
            <div class="pull-left">
                <button type="button" class="button-menu-mobile open-left waves-effect waves-light"><i
                            class="ion-navicon"></i>
                </button>
                <span class="clearfix"></span></div>
            <ul class="nav navbar-nav navbar-right pull-right">
                <li class="dropdown">
                    <a href="" class="dropdown-toggle profile waves-effect waves-light"
                       data-toggle="dropdown" aria-expanded="true">
                        {{--{{ dd(\Illuminate\Support\Facades\Auth::user()->member()->latest()->first()) }}--}}
                        @if(\Illuminate\Support\Facades\Auth::user()->member()->latest()->first() == null )
                            <img src="{{ Avatar::create(\Illuminate\Support\Facades\Auth::user()->email)->setDimension(200)->setFontSize(92)->toBase64()}}"
                                 alt="user-img" class="img-circle">
                        @else
                            <img src="{{\Illuminate\Support\Facades\Auth::user()->member()->latest()->first()->picture}}" alt="" class="img-circle">
                        @endif
                        <span class="profile-username"> {{\Illuminate\Support\Facades\Auth::user()->member()->first() == null ? \Illuminate\Support\Facades\Auth::user()->email : \Illuminate\Support\Facades\Auth::user()->member()->first()->first_name }} <br/>
                            <small>
                               {{ \Illuminate\Support\Facades\Auth::user()->roles()->first()->display_name }}
                            </small>
                        </span> </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('logout')}}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
