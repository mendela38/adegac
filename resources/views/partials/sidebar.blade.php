<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <div class="user-details">
            <div class="text-center">
                @if(\Illuminate\Support\Facades\Auth::user()->member()->latest()->first() == null )
                    <img src="{{ Avatar::create(\Illuminate\Support\Facades\Auth::user()->email)->setDimension(200)->setFontSize(92)->toBase64()}}"
                         alt="user-img" class="img-circle">
                @else
                    <img src="{{ \Illuminate\Support\Facades\Auth::user()->member()->latest()->first()->picture }}"
                         alt="" class="img-circle">
                @endif
            </div>
            <div class="user-info">
                <div class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                         aria-expanded="false"> {{ \Illuminate\Support\Facades\Auth::user()->roles()->first()->display_name }}</a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"> Logout
                            </a>
                        </li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </ul>
                </div>
                <p class="text-white m-0"><i class="fa fa-dot-circle-o text-success"></i> Online</p></div>
        </div>
        <div id="sidebar-menu">
            <ul>
                @permission('browse-people|edit-people|add-people|browse-contributions|edit-contributions')
                {{--Members--}}
                <li class="">
                    <a href="{{ route('people.index') }}"
                       class="waves-effect {{ Nav::isResource('people') }}{{ Nav::isResource('contributions') }}">
                        <i class="fa fa-4x fa-group" aria-hidden="true"></i>
                        <span> Community</span>
                    </a>
                </li>
                @endpermission
                @permission('browse-events|edit-events|add-events')
                {{--Events--}}
                <li class="">
                    <a href="{{route('events.index')}}"
                       class="waves-effect {{ Nav::isResource('events') }}{{ Nav::isResource('finances') }}"><i
                            class="fa fa-calendar"></i>
                        <span> Calendar </span></a>
                </li>
                @endpermission
                @permission('browse-reports|edit-reports|add-reports')
                {{--Reports--}}
                <li><a class="waves-effect {{ Nav::isResource('reports') }}" href="{{ route('reports.index') }}">
                        <i class="fa fa-files-o"></i> Reports</a>
                </li>
                @endpermission
                @permission('browse-bureaus|edit-bureaus|add-bureaus')
                {{--Bureau--}}
                <li><a class="waves-effect {{ Nav::isResource('bureau') }}" href="{{ route('bureau.index') }}">
                        <i class="fa fa-users"></i> Bureau</a>
                </li>
                @endpermission
                @permission('browse-departments|edit-departments|add-departments')
                {{--Departments--}}
                <li><a class="waves-effect {{ Nav::isResource('departments') }}"
                       href="{{ route('departments.index') }}">
                        <i class="fa fa-briefcase"></i> Departments</a>
                </li>
                @endpermission
                @permission('browse-users|edit-users|add-users')
                {{--Users--}}
                <li><a class="waves-effect {{ Nav::isResource('users') }}" href="{{ route('users.index') }}">
                        <i class="fa fa-user-secret"></i> Users</a>
                </li>
                @endpermission
                @permission('browse-meetings|edit-meetings|add-meetings')
                {{--Meetings--}}
                <li><a class="waves-effect {{ Nav::isResource('meetings') }}" href="{{ route('meetings.index') }}">
                        <i class="fa fa-calendar fa-plus"></i> Meetings</a>
                </li>
                @endpermission
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
