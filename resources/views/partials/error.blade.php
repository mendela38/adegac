@if($errors->any())
    <div class="alert alert-warning message">

            @foreach($errors->all() as $error)
                {{$error}}<br>
            @endforeach

    </div>
@elseif (session('success'))
    <div class="alert alert-info text-center message">
        <strong>{{ session('success') }}</strong>
    </div>
    <script>
        window.setTimeout(function () {
            $(".message").fadeTo(700, 0).slideUp(500, function () {
                $(this).remove();
            });
        }, 5000);
    </script>
@elseif(session('warning'))
    <div class="alert alert-warning text-center message">
        <strong>{{ session('warning') }}</strong>
    </div>
    <script>
        window.setTimeout(function () {
            $(".message").fadeTo(700, 0).slideUp(500, function () {
                $(this).remove();
            });
        }, 5000);
    </script>
@elseif(session('status'))
    <div class="alert alert-warning text-center message">
        <strong>{{ session('status') }}</strong>
    </div>
@endif
