@extends('layouts.authLayout')

@section('pageheader')
Sign In
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-horizontal" method="POST" action="{{ route('login') }}">

            {{csrf_field()}}
            @include('partials.error')
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="">Email Address</label>
                <input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email"
                       required autofocus autocomplete="on">
            </div>

            <div class="form-group{{ $errors->has('password')? ' has-error' : '' }}">
                <label for="">Password</label>
                <input id="password" type="password" class="form-control" name="password" placeholder="Password"
                       required>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-xs-12">
                    <div class="">
                        <label>
                            <input type="checkbox" data-toggle="toggle"
                                   name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                        </label>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <a href="{{ route('password.request') }}" class="text-muted pull-right"><i
                                class="fa fa-lock m-r-5"></i> Reset Password </a>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-primary btn-block btn-lg waves-effect waves-light" type="submit">Log In
                </button>
            </div>
        </form>
    </div>


@endsection
