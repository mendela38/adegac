@extends('layouts.master')

@section('title')
    Verify Account
@endsection

@section('content')
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3>{{ __('Verify Your Email Address') }}</h3>
                        </div>
                        <div class="panel-body text-center">

                            @if (session('resent'))
                                <div class="alert alert-success" role="alert">
                                    {{ __('A fresh verification link has been sent to your email address.') }}
                                </div>
                            @endif

                            <h5>{{ __('Before proceeding, please check your email for a verification link.') }}</h5>
                           <h5> {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
