@extends('layouts.authLayout')

@section('pageheader')
    Request Password change
@endsection

@section('content')
    @include('partials.error')
    <form class="form-horizontal m-t-20" method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <div class="col-lg-12">
                <input id="email" type="email" class="form-control" name="email" placeholder="Your email address" autocomplete="off"
                       value="{{ old('email') }}" required title="">
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-12 ">
                <a href="{{ route('login') }}" class="text-muted pull-left"><i
                            class="fa fa-arrow-circle-left m-r-1"></i> Back to Login</a>
                <button type="submit" class="btn btn-primary waves-light waves-effect pull-right">
                    Request
                </button>
            </div>
        </div>
    </form>


@endsection
