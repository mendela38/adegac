@extends('layouts.master')

@section('title')
    Bureaus
@endsection

@section('pageScript')
    <link href="{{asset('/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection


@section('content')
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="form-group text-right">
                @permission('add-bureaus')
                <a href="{{route('bureau.create')}}" class="btn btn-md btn-dark waves-light waves-effect"><i
                        class="fa fa-plus-circle" title="Add Member"></i> Add Bureau
                </a>
                @endpermission
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            @include('partials.error')


                            <table id="bureau_table" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Bureau's Name</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th># Members</th>
                                    @permission('read-bureaus|edit-bureaus|delete-bureaus')
                                    <th></th>
                                    @endpermission
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($bureaus as $index => $bureau)

                                    <tr>
                                        <td>{{ $index+1 }}</td>
                                        <td>{{$bureau->display_name}}</td>
                                        <td>{{\Carbon\Carbon::parse($bureau->start_date)->toDateString()}}</td>
                                        <td>{{ $bureau->end_date == null ? 'Mandat will end on '. \Carbon\Carbon::parse($bureau->start_date)->addMonths(12)->toDateString() : \Carbon\Carbon::parse($bureau->start_date)->toDateString() }}</td>
                                        <td>{{ $bureau->users()->count() }}</td>
                                        @permission('read-bureaus|edit-bureaus|delete-bureaus')
                                        <td class="float-right">
                                            @permission('read-bureaus')
                                            @if($bureau->users()->count() !== 0)
                                            <a href="{{ route('bureau.show', ['bureau' => $bureau]) }}"
                                               class="btn btn-xs btn-info waves-effect">
                                                <i class="fa fa-eye" title="Edit Bureau"></i>
                                            </a>
                                            @endif
                                            @endpermission
                                            @permission('edit-bureaus')
                                            <a href="{{ route('bureau.edit', ['bureau' => $bureau]) }}"
                                               class="btn btn-xs btn-success waves-effect"><i
                                                    class="fa fa-pencil"></i>
                                            </a>
                                            @endpermission
                                            @permission('|delete-bureaus')
                                            <a href="{{ route('bureau.destroy', ['bureau' => $bureau]) }}"
                                               onclick="deleting('#delete-bureau{{$bureau->id}}', 'Bureau')"
                                               class="btn btn-xs btn-danger waves-effect"><i
                                                    class="fa fa-trash" title="Delete Member"></i>
                                            </a>
                                            <form class="hidden"
                                                  action="{{ route('bureau.destroy', ['bureau' => $bureau]) }}"
                                                  method="POST" id="delete-bureau{{$bureau->id}}">
                                                {{ @csrf_field() }}
                                                {{ method_field("DELETE") }}
                                            </form>
                                            @endpermission
                                        </td>
                                        @endpermission
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/responsive.bootstrap.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#bureau_table').DataTable({
                stateSave: true,
                "ordering": true,
                language: {
                    searchPlaceholder: "Searching...."
                },
                dom: 'frtp',
                responsive: true
            });
        });
    </script>
@endsection
