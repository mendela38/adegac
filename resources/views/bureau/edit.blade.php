@extends('layouts.master')

@section('title')
    Edit Bureau
@endsection

@section('pageScript')
    <link href="{{asset('/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection


@section('content')
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-body">
                            <h3 class="page-header text-center">Edit Bureau - {{ $bureau->display_name }}</h3>
                            @include('partials.error')

                            @include('partials.formMessage')


                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <form class="form-horizontal" role="form" method="POST"
                                          action="{{ route('bureau.update', ['bureau' => $bureau]) }}"
                                          enctype="multipart/form-data">

                                        {{csrf_field()}}
                                        {{ method_field("PATCH") }}

                                        <div class="container">
                                            <div
                                                class="form-group{{$errors->has('name') ? ' has-error' : ' has-warning'}}">
                                                <label for="">Bureau Name</label>
                                                <input type="text" class="form-control" name="name"
                                                       placeholder="Bureau Name" autocomplete="off"
                                                       value="{{old('name') ? old('name') : $bureau->display_name}}">
                                            </div>

                                            <div
                                                class="form-group{{$errors->has('first_name') ? ' has-error' : ''}}">
                                                <label for="">Description</label>
                                                <textarea name="description" id="description" class="form-control"
                                                          rows="3"
                                                          placeholder="Description">{{ old('description') ? old('description') : $bureau->description }}</textarea>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-6 ">
                                                    <div class="form-group">
                                                        <label for="">Starting Date</label>
                                                        <div
                                                            class=" input-group{{$errors->has('start_date') ? ' has-error' : ' has-warning'}}">

                                                            <input type="text" class="form-control date"
                                                                   name="start_date"
                                                                   placeholder="Starting Date" autocomplete="off"
                                                                   value="{{old('start_date') ? old('start_date') : \Carbon\Carbon::parse($bureau->start_date)->toDateString()}}"
                                                                   readonly>
                                                            <span class="input-group-addon bg-custom b-0"><i
                                                                    class="mdi mdi-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <label for="">Ending Date</label>
                                                    <div
                                                        class="input-group{{$errors->has('end_date') ? ' has-error' : ''}}">

                                                        <input type="text" class="form-control date" name="end_date"
                                                               placeholder="Ending Date" autocomplete="off"
                                                               value="{{old('end_date') ? old('end_date') : $bureau->end_date == null ? \Carbon\Carbon::parse($bureau->start_date)->addMonths(12)->toDateString() : \Carbon\Carbon::parse($bureau->start_date)->toDateString() }}"
                                                               readonly>
                                                        <span class="input-group-addon bg-custom b-0"><i
                                                                class="mdi mdi-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <div class=" text-right">
                                                    <button type="submit" class="btn btn-dark "><i
                                                            class="fa fa-save"></i> Save
                                                    </button>
                                                    <a href="{{ route('bureau.index') }}" class="btn btn-default"><i
                                                            class="fa fa-reply"></i> Back</a>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')
    <script src="{{asset('/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        datePicker();
    </script>
@endsection
