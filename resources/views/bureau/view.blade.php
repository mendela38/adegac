@extends('layouts.master')

@section('title')
    View Bureau
@endsection


@section('content')

    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h3 class="page-header text-center mt-0">{{ $bureau->display_name }} - Bureau
                                    Information
                                    <a href="{{ route('bureau.index') }}" class="btn btn-default pull-right"> <i
                                            class="fa fa-reply"></i> Bureaus</a></h3>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        @foreach($bureau->users()->get() as $user)
                                        <div class="col-md-4">
                                            <div class="panel">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <img
                                                                src="{{ Avatar::create($user->fullName())->setDimension(75)->setFontSize(35)->toBase64()}}"
                                                                class="img img-circle"
                                                                alt="">
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="">
                                                                <label for="">Name:</label>
                                                                <small>{{ $user->fullName() }}</small>
                                                            </div>
                                                            <div class="">
                                                                <label for="">Role:</label>
                                                                <small>{{ $user->roles()->first()->display_name }}</small>
                                                            </div>
                                                            <a href="{{ route('people.show', ['person' => $user->member()->first()]) }}">View Information</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script>
        $(document).ready(function () {
            $("span:contains('Administration')").parent('a').addClass('active').click().addClass('subdrop');
        });
    </script>
@endsection

