<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>{{ config('app.name') }} - @yield('pageheader')</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="Admin Dashboard" name="description"/>
    <meta content="ThemeDesign" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="shortcut icon" href="{{ asset('/images/Adegac.jpg') }}">
    <link href="{{asset('/css/bootstrap.min.css')}} " rel="stylesheet" type="text/css">
    <link href="{{asset('/css/icons.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/css/style.css')}}" rel="stylesheet" type="text/css">
</head>
<body>
<div class="accountbg"></div>
<div class="wrapper-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default" style="opacity: 0.98">
                    <div class="panel-heading">

                        <div style="text-align: center">
                            <img src="{{asset('/images/Cape_logo.png')}}" style="width: 50%; " alt="Adegac">
                            <h1 class="m-0"><strong>
                                    <span style="color: #0cb800">AD</span><span style="color: #f2f000">EG</span><span style="color: blue">AC</span>
                                    </strong></h1>
                            <small><strong>Pour une jeunesse Consciente et Differente</strong></small>
                        </div>
                    </div>

                    <div class="panel-body">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script src="{{asset('/js/jquery.min.js')}}"></script>
<script src="{{asset('/js/bootstrap.min.js')}}"></script>
<script src="{{asset('/js/waves.js')}}"></script>
</body>
</html>
