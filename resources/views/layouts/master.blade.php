<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="Adegac Admin" name="description"/>
    <meta content="Author" name="Nelson Dick Kelechi"/>
    <meta content="Website" name="http://www.nelsonkelechi.com"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }} - @yield('title')</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="Admin Dashboard" name="description"/>
    <meta content="ThemeDesign" name="Nelson"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="shortcut icon" href="{{ asset('/images/Adegac.jpg') }}">
    <link href="{{asset('/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/css/icons.css')}}" rel="stylesheet" type="text/css">

    <link href="{{asset('/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet" type="text/css">

    @yield('pageScript')
    <link href="{{asset('/css/style.css')}}" rel="stylesheet" type="text/css">
</head>
<body class="fixed-left">
<div id="wrapper">
    <div class="topbar">
        @include('partials.nav')
    </div>

    @include('partials.sidebar')

    <div class="content-page">
        <div class="content">
            @yield('content')
        </div>
        <div class="footer">
            <small class="pull-left">Designed by <a href="http://www.nelsonkelechi.com/" target="_blank">Nelson Dick</a></small>
            <footer class="pull-right"> &copy; ADEGAC 2005 - <?php echo date("Y");?> | For the community</footer>
        </div>
    </div>
</div>
<script src="{{asset('/js/jquery.min.js')}}"></script>
<script src="{{asset('/js/bootstrap.min.js')}}"></script>
<script src="{{asset('/js/modernizr.min.js')}}"></script>
<script src="{{asset('/js/detect.js')}}"></script>
<script src="{{asset('/js/fastclick.js')}}"></script>
<script src="{{asset('/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('/js/waves.js')}}"></script>
<script src="{{asset('/js/wow.min.js')}}"></script>
<script src="{{asset('/js/app.js')}}"></script>
<script src="{{asset('/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}"></script>
<script src="{{asset('/js/adegac.js')}}"></script>
@yield('script')
</body>
</html>
