@extends('layouts.master')

@section('title')
    Departments
@endsection

@section('pageScript')
    <link href="{{asset('/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection


@section('content')
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group text-right">
                        @permission('add-departments')
                        <a href="{{route('departments.create')}}" class="btn btn-md btn-dark waves-light waves-effect"><i
                                    class="fa fa-plus-circle" title="Add Department"></i> Add Department
                        </a>
                        @endpermission
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            @include('partials.error')

                            <table id="department_table" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Last Update</th>
                                    @permission('edit-departments|delete-departments')
                                    <th>Actions</th>
                                    @endpermission
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($departments as $index => $department)
                                    <tr>
                                        <td>{{ $index+1 }}</td>
                                        <td>{{$department->display_name}}</td>
                                        <td>{{ \Carbon\Carbon::parse($department->updated_at)->toDayDateTimeString() }}</td>
                                        @permission('edit-departments|delete-departments')
                                        <td class="float-right">
                                            @permission('edit-departments')
                                            <a href="{{ route('departments.edit', ['department' => $department]) }}" class="btn btn-xs btn-success waves-effect"><i
                                                        class="fa fa-pencil"></i>
                                            </a>
                                            @endpermission
                                            @permission('delete-departments')
                                            <a href="{{ route('departments.destroy', ['department' => $department]) }}" onclick="deleting('#delete-department{{$department->id}}', 'Department')"
                                               class="btn btn-xs btn-danger waves-effect"><i
                                                        class="fa fa-trash"></i>
                                            </a>
                                            <form class="hidden" action="{{ route('departments.destroy', ['department' => $department]) }}" method="POST" id="delete-department{{$department->id}}">
                                                {{ @csrf_field() }}
                                                {{ method_field("DELETE") }}
                                            </form>
                                            @endpermission
                                        </td>
                                        @endpermission
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/responsive.bootstrap.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#department_table').DataTable({
                stateSave: true,
                "ordering": true,
                language: {
                    searchPlaceholder: "Searching...."
                },
                dom: 'frtp',
                responsive: true
            });
        });
    </script>
@endsection
