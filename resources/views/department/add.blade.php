@extends('layouts.master')

@section('title')
    Add Department
@endsection

@section('pageScript')
    <link rel="stylesheet" href="{{asset('/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css')}}">
@endsection

@section('content')
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <h3 class="page-header text-center">Department Information</h3>
                            @include('partials.error')

                            @include('partials.formMessage')


                            <form class="form-horizontal" role="form" method="POST"
                                  action="{{ route('departments.store') }}">

                                {{csrf_field()}}

                                <div class="row">
                                    <div class="col-lg-offset-1 col-lg-10 col-xs-12">

                                        <div class="form-group{{$errors->has('name') ? ' has-error' : ' has-warning'}}">
                                            <label for="">Department Name</label>
                                            <input type="text" class="form-control" name="name"
                                                   placeholder="Department Name" autocomplete="off"
                                                   value="{{old('name')}}">
                                        </div>

                                        <div class="form-group{{$errors->has('description') ? ' has-error' : ''}}">
                                            <label for="">Description</label>
                                            <textarea name="description" id="description"
                                                      placeholder="Description" class="form-control"
                                                      rows="3">{{ old('description') }}</textarea>
                                        </div>

                                        @foreach($permissionCategories as $permissionCategory)

                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <h4 class="form-check-label">{{ucfirst(str_replace('_', ' ', $permissionCategory))}}</h4>
                                                </div>
                                                <div class="col-md-12 row">
                                                    @foreach($permissions as $count => $permission)
                                                        @if(strpos($permission->name, $permissionCategory) !== false)
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="checkbox-inline ">
                                                                        <input type="checkbox"
                                                                               data-toggle="toggle"
                                                                               data-onstyle="success"
                                                                               data-offstyle="danger"
                                                                               data-size="small"
                                                                               name="permissions[]"
                                                                               value="{{$permission->id}}"
                                                                        >{{ucfirst($permission->display_name)}}
                                                                    </label>
                                                                </div>

                                                            </div>
                                                        @endif
                                                    @endforeach
                                                </div>

                                            </div>
                                        @endforeach

                                        <div class="form-group">
                                            <div class=" text-right">
                                                <button type="submit" class="btn btn-dark "><i
                                                        class="fa fa-plus-circle"></i> Add
                                                </button>
                                                <a href="{{ route('departments.index') }}" class="btn btn-default"><i
                                                        class="fa fa-reply"></i> Back</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js')}}"></script>
@endsection
