@extends('layouts.master')

@section('title')
    Edit Member
@endsection

@section('pageScript')
    <link href="{{asset('/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection


@section('content')
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            @include('partials.error')

                            @include('partials.formMessage')

                            <div class="row">
                                <h3 class="page-header text-center">Personal Information</h3>
                                <form class="form-horizontal" role="form" method="POST"
                                      action="{{ route('people.update', $person) }}"
                                      enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    {{ method_field('PATCH') }}
                                    <div class="col-lg-2 col-xs-12 pt-5">

                                        <img src="{{ $person->picture == null ? asset('/images/users/avatar-1.jpg') : $person->picture}}"
                                             style="border: solid;display: block; margin: auto; width: 60%; padding: 10px;"
                                             alt="user">
                                        <input id="upload" type="file" class="filestyle" name="picture"
                                               data-iconname="fa fa-cloud-upload" autocomplete="off"
                                               accept="image/png,image/jpeg"
                                               value="">
                                    </div>
                                    <div class="col-lg-9">




                                        <div class="row">
                                            <div class="col-lg-6 col-xs-12">
                                                <div class="form-group{{$errors->has('last_name') ? ' has-error' : ' has-warning'}}">
                                                    <label for="">Last Name</label>
                                                    <input type="text" class="form-control" name="last_name"
                                                           placeholder="Last Name" autocomplete="off"
                                                           value="{{old('last_name') ? old('last_name') : $person->last_name}}">
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-xs-12">
                                                <div class="form-group{{$errors->has('first_name') ? ' has-error' : ' has-warning'}}">
                                                    <label for="">First Name</label>
                                                    <input type="text" class="form-control" name="first_name"
                                                           placeholder="First Name" autocomplete="off"
                                                           value="{{old('first_name') ? old('first_name') : $person->first_name}}">
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-xs-12">
                                                <label for="">Date of Birth</label>
                                                <div class="form-group input-group{{$errors->has('date_of_birth') ? ' has-error' : ' has-warning'}}">

                                                    <input type="text" class="form-control date" name="date_of_birth"
                                                           placeholder="Date of Birth" autocomplete="off"
                                                           value="{{old('date_of_birth') ? old('date_of_birth') : $person->dob }}"
                                                           readonly>
                                                    <span class="input-group-addon bg-custom b-0"><i
                                                                class="mdi mdi-calendar"></i></span>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-xs-12">
                                                <div class="form-group ">
                                                    <label for="">Place of Birth</label>
                                                    <input type="text" class="form-control" name="place_of_birth"
                                                           placeholder="Place of Birth" autocomplete="off"
                                                           value="{{old('place_of_birth') ? old('place_of_birth') : $person->pob }}">
                                                </div>
                                            </div>

                                            <div class="col-lg-2 col-xs-12">
                                                <div class="form-group{{$errors->has('gender') ? ' has-error' : ' has-warning'}}">
                                                    <label for="">Gender</label>
                                                    <select name="gender" id="" class="form-control">
                                                        <option value="">-- Select Gender --</option>
                                                        <option value="Male" {{old('gender') === 'Male' ? 'selected' : $person->gender === 'Male'? 'selected':''}}>
                                                            Male
                                                        </option>
                                                        <option value="Female" {{old('gender') === 'Female' ? 'selected' : $person->gender === 'Female'? 'selected':''}}>
                                                            Female
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="col-lg-4 col-xs-12">
                                                <div class="form-group{{$errors->has('passport_number') ? ' has-error' : ' has-warning'}}">
                                                    <label for="">Passport Number</label>
                                                    <input type="text" class="form-control" name="passport_number"
                                                           placeholder="Passport Number" autocomplete="off"
                                                           value="{{old('passport_number') ? old('passport_number') : $person->passport_number }}">
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-xs-12">
                                                <div class="form-group{{$errors->has('occupation_title') ? ' has-error' : ' has-warning'}}">
                                                    <label for="">Occupation</label>
                                                    <select name="occupation_title" id="" class="form-control">
                                                        <option value="">-- Select Occupation --</option>
                                                        <option value="Student" {{old('occupation_title') === 'Student' ? 'selected' : $person->occupation_title === 'Student'? 'selected':''}}>
                                                            Student
                                                        </option>
                                                        <option value="Worker" {{old('occupation_title') === 'Worker' ? 'selected' : $person->occupation_title === 'Worker'? 'selected':''}}>
                                                            Worker
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-xs-12">
                                                <div class="form-group ">
                                                    <label for="">Course or Carrer</label>
                                                    <input type="text" class="form-control" name="occupation_type"
                                                           placeholder="Course or Career"
                                                           autocomplete="off"
                                                           value="{{old('occupation_type') ? old('occupation_type') : $person->occupation_type }}">
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-xs-12">
                                                <div class="form-group ">
                                                    <label for="">Institution name or Company Name</label>
                                                    <input type="text" class="form-control" name="occupation_place"
                                                           placeholder="Institution name or Company name"
                                                           autocomplete="off"
                                                           value="{{old('occupation_place') ? old('occupation_place') : $person->occupation_place }}">
                                                </div>
                                            </div>

                                            <div class="col-lg-12 col-xs-12">
                                                <div class="form-group{{$errors->has('address') ? ' has-error' : ' has-warning'}}">
                                                    <label for="">Address</label>
                                                    <input type="text" class="form-control" name="address"
                                                           autocomplete="off" placeholder="Full Address"
                                                           value="{{old('address') ? old('address') : $person->address}}">
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-xs-12">
                                                <div class="form-group{{$errors->has('email') ? ' has-error' : ' has-warning'}}">
                                                    <label for="">Email</label>
                                                    <input type="email" class="form-control" name="email"
                                                           placeholder="Email"
                                                           value="{{old('email') ? old('email') : $person->email }}">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-xs-12">
                                                <div class="form-group{{$errors->has('cell_number') ? ' has-error' : ' has-warning'}}">
                                                    <label for="">Contact Number</label>
                                                    <input type="text" class="form-control" name="cell_number"
                                                           autocomplete="off" placeholder="Contact Number"
                                                           value="{{old('cell_number') ? old('cell_number') : $person->cell_number }}">
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-xs-12">
                                                <label for="">Joining Date</label>
                                                <div class="form-group input-group{{$errors->has('joining_date') ? ' has-error' : ' has-warning'}}">

                                                    <input type="text" class="form-control date" name="joining_date"
                                                           autocomplete="off" placeholder="Joining Date"
                                                           value="{{old('joining_date') ? old('joining_date') : $person->joining_date}}"
                                                           readonly>
                                                    <span class="input-group-addon bg-custom b-0"><i
                                                                class="mdi mdi-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <h3 class="page-header text-center">Next Kin's information</h3>

                                            <div class="col-lg-4 col-xs-12">
                                                <div class="form-group{{$errors->has('nk_last_name') ? ' has-error' : ' has-warning'}}">
                                                    <label for="">Last Name</label>
                                                    <input type="text" class="form-control" name="nk_last_name"
                                                           autocomplete="off" placeholder="Last Name"
                                                           value="{{old('nk_last_name') ? old('nk_last_name') : $person->nk_last_name }}">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-xs-12">
                                                <div class="form-group{{$errors->has('nk_first_name') ? ' has-error' : ' has-warning'}}">
                                                    <label for="">First Name</label>
                                                    <input type="text" class="form-control" name="nk_first_name"
                                                           autocomplete="off" placeholder="First Name"
                                                           value="{{old('nk_first_name') ? old('nk_first_name') : $person->nk_first_name }}">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-xs-12">
                                                <div class="form-group{{$errors->has('nk_contact') ? ' has-error' : ' has-warning'}}">
                                                    <label for="">Contact Number</label>
                                                    <input type="text" class="form-control" name="nk_contact"
                                                           autocomplete="off" placeholder="Contact Number"
                                                           value="{{old('nk_contact') ? old('nk_contact') : $person->nk_contact }}">
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-xs-12">
                                                <div class="form-group">
                                                    <div class=" text-right">
                                                        @permission('edit-people')
                                                        <button type="submit" class="btn btn-dark ">Update Member</button>
                                                        @endpermission
                                                        @permission('browse-people')
                                                        <a href="{{ route('people.index') }}" class="btn btn-default"><i class="fa fa-reply"></i> Back</a>
                                                        @endpermission
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')
    <script src="{{asset('/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}"
            type="text/javascript"></script>
    <script>datePicker();</script>
@endsection
