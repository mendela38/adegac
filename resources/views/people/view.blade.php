@extends('layouts.master')

@section('title')
    View Member's Profile
@endsection

@section('pageScript')
    <link href="{{asset('/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            @include('partials.error')
                            <div class="text-right">
                                @permission('browse-people')
                                <a href="{{ route('people.index') }}"
                                   class="btn btn-sm btn-dark waves-light waves-effect"><i class="fa fa-reply"></i>
                                    &nbsp;Back &nbsp;</a>
                                @endpermission
                                @permission('edit-people')
                                <a href="{{ route('people.edit', ['person' => $person]) }}"
                                   class="btn btn-sm btn-success waves-light waves-effect"><i class="fa fa-pencil"></i>
                                    &nbsp;Edit &nbsp;</a>
                                @endpermission
                                @permission('delete-people')
                                <a href="{{ route('people.destroy', ['person' => $person]) }}" onclick="deleting('#delete-person{{$person->id}}', 'Meeting')"
                                   class="btn btn-sm btn-danger waves-effect"><i
                                        class="fa fa-trash" title="Delete Member"></i> Delete
                                </a>
                                <form class="hidden" action="{{ route('people.destroy', ['person' => $person]) }}" method="POST" id="delete-person{{$person->id}}">
                                    {{ @csrf_field() }}
                                    {{ method_field("DELETE") }}
                                </form>
                                @endpermission
                            </div>

                            <div class="panel-heading">
                                <h3 class="page-header text-center">Information</h3>
                            </div>

                            <div class="row">
                                <div class="col-lg-2 col-xs-12" style="margin-bottom: 10px">
                                    @if($person->picture == null)
                                        <img src="{{asset('/images/users/avatar-1.jpg')}}"
                                         style="  border: solid;display: block; margin: auto; width: 60%; padding: 10px;" alt="user img">
                                    @else
                                        <img src="{{ $person->picture }}" alt="{{$person->last_name}}"
                                             style="  border: solid;display: block; margin: auto; width: 60%; padding: 10px;">
                                    @endif
                                </div>
                                <div class="col-lg-9">
                                    <div class="col-lg-6 col-xs-12">
                                        <div class="form-group ">
                                            <label for="last_name">Last Name</label>
                                            <input type="text" class="form-control" value="{{$person->last_name}}" readonly id="last_name">
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text" class="form-control" value="{{$person->first_name}}" id="" readonly>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-xs-12">
                                        <div class="form-group ">
                                            <label for="">Date of Birth</label>
                                            <input type="text" class="form-control" value="{{$person->dob}}" id="" readonly>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-xs-12">
                                        <div class="form-group ">
                                            <label for="">Place of Birth</label>
                                            <input type="text" class="form-control" value="{{$person->pob}}" id="" readonly>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-xs-12">
                                        <div class="form-group ">
                                            <label for="">Gender</label>
                                            <input type="text" class="form-control" value="{{$person->gender}}" id="" readonly>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-xs-12">
                                        <div class="form-group ">
                                            <label for="">Passport number</label>
                                            <input type="text" class="form-control" value="{{$person->passport_number}}" id="" readonly>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-xs-12">
                                        <div class="form-group">
                                            <label for="">Occupation</label>
                                            <input type="text" class="form-control" value="{{$person->occupation_title}}" id="" readonly>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-xs-12">
                                        <div class="form-group">
                                            <label for="">Course or Career</label>
                                            <input type="text" class="form-control" value="{{$person->occupation_type === null ? 'Not Specified' : $person->occupation_type}}" id="" readonly>
                                        </div>
                                    </div>

                                    <div class="col-lg-5 col-xs-12">
                                        <div class="form-group ">
                                            <label for="">Institution name or Company Name</label>
                                            <input type="text" class="form-control" value="{{$person->occupation_place === null ? 'Not Specified' : $person->occupation_place}}" id="" readonly>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-xs-12">
                                        <div class="form-group ">
                                            <label for="">Address</label>
                                            <input type="text" class="form-control" value="{{$person->address}}" id="" readonly>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-xs-12">
                                        <div class="form-group ">
                                            <label for="">Email Address</label>
                                            <input type="text" class="form-control" value="{{$person->email}}" id="" readonly>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-xs-12">
                                        <div class="form-group ">
                                            <label for="">Contact Number</label>
                                            <input type="text" class="form-control" value="{{$person->cell_number}}" id="" readonly>
                                        </div>
                                    </div>


                                    <div class="col-lg-4 col-xs-12">
                                        <div class="form-group ">
                                            <label for="">Joining date</label>
                                            <input type="text" class="form-control" value="{{$person->joining_date == null ? 'None' : $person->joining_date}}" id="" readonly>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-xs-12">
                                        <div class="form-group">
                                            <label for="">Next Kin's Last Name</label>
                                            <input type="text" class="form-control" value="{{$person->nk_last_name}}" id="" readonly>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-xs-12">
                                        <div class="form-group">
                                            <label for="">Next Kin's First Name</label>
                                            <input type="text" class="form-control" value="{{$person->nk_first_name}}" id="" readonly>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-xs-12">
                                        <div class="form-group">
                                            <label for="">Next Kin's Contact Number</label>
                                            <input type="text" class="form-control" value="{{$person->nk_contact}}" id="" readonly>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            @permission('edit-finances')
                            <div class="row">
                                <h3 class="page-header text-center">Financial History</h3>

                                <div class="row">
                                    <div class="col-lg-offset-2 col-lg-9 col-xs-12">
                                        <table id="financial_history_table" class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                                <th>Amount Paid</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($person->financialHistory as $index => $finances)
                                                <tr>
                                                    <td>{{$index+1}}</td>
                                                    <td>{{$finances->payment_date}}</td>
                                                    <td>{{$finances->amount}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @endpermission
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')
    <script src="{{asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/responsive.bootstrap.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $("span:contains('Community')").parent('a').addClass('active').click().addClass('subdrop');
            $('#financial_history_table').DataTable({
                stateSave: true,
                "ordering": false,
                language: {
                    searchPlaceholder: "Searching...."
                },
                dom: 'frtp',
                responsive: true
            });
        });
    </script>
@endsection
