@extends('layouts.master')

@section('title')
    Members
@endsection

@section('pageScript')
    <link href="{{asset('/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection


@section('content')

    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-lg-4 col-xs-4">
                    <div class="panel text-center">
                        <div class="panel-heading visible-lg"><h4 class="panel-title text-muted font-light">Total
                                Subscription</h4></div>
                        <div class="panel-body p-t-10">
                            <h1 class="m-t-0 m-b-0"><i
                                        class="mdi mdi-account-multiple text-danger m-r-10"></i><b>{{ \App\Member::countingMembers() }}</b>
                            </h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4 col-xs-4">
                    <div class="panel text-center">
                        <div class="panel-heading visible-lg"><h4 class="panel-title text-muted font-light">Total Male
                                Members</h4>
                        </div>
                        <div class="panel-body p-t-10">

                            <h1 class="m-t-0 m-b-0"><i class="mdi mdi-gender-male text-primary m-r-0"></i><b>
                                    {{ \App\Member::countingMembers('Male')  }}</b>
                            </h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4 col-xs-4">
                    <div class="panel text-center">
                        <div class="panel-heading visible-lg"><h4 class="panel-title text-muted font-light">Total Female
                                Members</h4></div>
                        <div class="panel-body p-t-10">
                            <h1 class="m-t-0 m-b-0"><i
                                        class="mdi mdi-gender-female  text-success m-r-0"></i><b>{{\App\Member::countingMembers('Female') }}</b>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            @include('partials.error')
                            <div class="form-group text-right">
                                @permission('browse-contributions')
                                <a href="{{route('contributions.index')}}"
                                   class="btn btn-md btn-dark waves-light waves-effect" style="margin-bottom: 10px;">
                                    <i
                                            class="fa fa-plus"></i> Member's Contribution
                                </a>
                                @endpermission
                                @permission('add-people')
                                <a href="{{route('people.create')}}"
                                   class="btn btn-md btn-dark waves-light waves-effect" style="margin-bottom: 10px;"><i
                                            class="fa fa-plus-circle" title="Add Member"></i> Add Member
                                </a>
                                @endpermission
                            </div>

                            <table id="person_table" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Full Name</th>
                                    <th>Passport No</th>
                                    <th>Contact No</th>
                                    <th>Email</th>
                                    @permission('read-people|edit-people|delete-people')
                                    <th style="width: 7%">Actions</th>
                                    @endpermission
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($people as $index => $person)
                                    <tr>
                                        <td style="width: 2%">{{ $index +1 }}</td>
                                        <td>{{$person->first_name}} <b>{{$person->last_name}}</b></td>
                                        <td>{{strtoupper($person->passport_number)}}</td>
                                        <td>{{$person->cell_number}}</td>
                                        <td>{{$person->email}}</td>
                                        @permission('read-people|edit-people|delete-people')
                                        <td class="float-right">
                                            @permission('read-people')
                                            <a href="{{ route('people.show', ['person' => $person]) }}"
                                               class="btn btn-xs btn-info waves-effect">
                                                <i class="fa fa-eye" title="Edit Member"></i>
                                            </a>
                                            @endpermission
                                            @permission('edit-people')
                                            <a href="{{ route('people.edit', ['person' => $person]) }}"
                                               class="btn btn-xs btn-success waves-effect"><i
                                                        class="fa fa-pencil"></i>
                                            </a>
                                            @endpermission
                                            @permission('delete-people')
                                            <a href="{{ route('people.destroy', ['person' => $person]) }}"
                                               onclick="deleting('#delete-person{{$person->id}}', 'Meeting')"
                                               class="btn btn-xs btn-danger waves-effect"><i
                                                        class="fa fa-trash" title="Delete Member"></i>
                                            </a>
                                            <form class="hidden"
                                                  action="{{ route('people.destroy', ['person' => $person]) }}"
                                                  method="POST" id="delete-person{{$person->id}}">
                                                {{ @csrf_field() }}
                                                {{ method_field("DELETE") }}
                                            </form>
                                            @endpermission
                                        </td>
                                        @endpermission
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/responsive.bootstrap.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.table').DataTable({
                stateSave: true,
                "ordering": false,
                language: {
                    searchPlaceholder: "Searching...."
                },
                dom: 'frtp',
                responsive: true
            });
        });
    </script>
@endsection
