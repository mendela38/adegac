@extends('layouts.master')

@section('title')
    {{ $event->title }}'s Financial Breakdown
@endsection

@section('pageScript')
    <link href="{{asset('/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="form-group pull-right">
                    <a href="{{ route('finances.index') }}" class="btn btn-dark"><i class="fa fa-reply"></i> Financial
                        Summary</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            @if (session('success') || session('warning'))
                                @include('partials.error')
                            @endif
                            <div class="col-lg-12" style="padding: 0;">
                                <div class="panel-group" id="accordion-test-2">
                                    <div class="panel panel-info panel-color">
                                        <div class="panel-heading"><h4 class="panel-title"><a
                                                    data-parent="#accordion-test-2" aria-expanded="false"
                                                    class="collapsed">Incomes Table </a></h4></div>

                                        <div id="income" class="panel-collapse collapse in">
                                            <div class="panel-body" style="padding: 0">


                                                <div class="col-md-offset-2 col-md-8" style="margin-top: 20px">
                                                    @if($errors->has('source') || $errors->has('type_of_income') || $errors->has('amount'))
                                                        @include('partials.error')
                                                    @endif
                                                    @permission('add-incomes')
                                                    <form role="form" method="POST"
                                                          action="{{ route('incomes.store') }}"
                                                          style="margin-bottom: 10px">

                                                        {{csrf_field()}}
                                                        <input type="hidden" name="event_id" value="{{ $event->id }}">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div
                                                                    class="form-group{{$errors->has('source') ? ' has-error' : ''}}">
                                                                    <input type="text" class="form-control"
                                                                           name="source" autocomplete="off"
                                                                           title=""
                                                                           placeholder="Source of Income"
                                                                           value="{{old('source')}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div
                                                                    class="form-group{{$errors->has('type_of_income') ? ' has-error' : ''}}">
                                                                    <label for="type_of_income"></label><select
                                                                        name="type_of_income" id="type_of_income"
                                                                        class="form-control"
                                                                        title="Type of Income">
                                                                        <option
                                                                            value="" {{old('type_of_income') === '' ? 'selected' : ''}}>
                                                                            -- Select type of income --
                                                                        </option>
                                                                        <option
                                                                            value="Sponsorship" {{old('type_of_income') === 'Sponsorship' ? 'selected' : ''}}>
                                                                            Sponsorship
                                                                        </option>
                                                                        <option
                                                                            value="Others" {{old('type_of_income') === 'Others' ? 'selected' : ''}}>
                                                                            Others
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div
                                                                    class="form-group{{$errors->has('amount') ? ' has-error' : ''}}">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">R</div>
                                                                        <input type="number"
                                                                               class="form-control"
                                                                               name="amount" autocomplete="off"
                                                                               title=""
                                                                               placeholder="Amount"
                                                                               value="{{old('amount')}}">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button type="submit"
                                                                        class="btn btn-md btn-dark btn-block"><i
                                                                        class="fa fa-plus-circle"></i> Add Income
                                                                </button>
                                                            </div>

                                                        </div>
                                                    </form>
                                                    @endpermission
                                                    <table id="income_table" class="table table-bordered table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th style="width: 10px">#</th>
                                                            <th>Origin</th>
                                                            <th>Type of income</th>
                                                            <th>Amount</th>
                                                            @permission('delete-incomes')
                                                            <th style="width: 10px"></th>
                                                            @endpermission
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($incomes as $index => $income)
                                                            <tr id="{{$income->id}}">
                                                                <td>{{ $index+1 }}</td>
                                                                <td>{{$income->source}}</td>
                                                                <td>{{$income->income_type}}</td>
                                                                <td class="text-right">
                                                                    R {{number_format($income->amount, 2)}}</td>
                                                                @permission('delete-incomes')
                                                                <td>
                                                                    <a href="{{ route('incomes.destroy', ['income' => $income]) }}"
                                                                       onclick="deleting('#delete-income{{$income->id}}', 'Income')"
                                                                       class="btn btn-xs btn-danger waves-effect"><i
                                                                            class="fa fa-trash"
                                                                            title="Delete income"></i>
                                                                    </a>
                                                                    <form class="hidden"
                                                                          action="{{ route('incomes.destroy', ['income' => $income]) }}"
                                                                          method="POST"
                                                                          id="delete-income{{$income->id}}">
                                                                        {{ @csrf_field() }}
                                                                        {{ method_field("DELETE") }}
                                                                    </form>
                                                                </td>
                                                                @endpermission
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-info panel-color">
                                        <div class="panel-heading"><h4 class="panel-title"><a
                                                    data-parent="#accordion-test-2"
                                                    class="collapsed"
                                                    aria-expanded="false"> Expense Table </a></h4>
                                        </div>
                                        <div id="expense" class="panel-collapse collapse in">
                                            <div class="panel-body" style="padding: 0">
                                                <div class="col-md-offset-2 col-md-8" style="margin-top: 20px">
                                                    @if($errors->has('item') || $errors->has('price'))
                                                        @include('partials.error')
                                                    @endif
                                                    @permission('add-expenses')
                                                    <form method="POST"
                                                          action="{{ route('expenses.store') }}"
                                                          style="margin-bottom: 10px">
                                                        {{csrf_field()}}

                                                        <input type="hidden" name="event_id" value="{{ $event->id }}">
                                                        <div class="row">
                                                            <div class="col-md-7">
                                                                <div
                                                                    class="form-group{{$errors->has('item') ? ' has-error' : ''}}">
                                                                    <input type="text" class="form-control"
                                                                           name="item" autocomplete="off"
                                                                           title=""
                                                                           placeholder="Item"
                                                                           value="{{old('item')}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div
                                                                    class="form-group{{$errors->has('price') ? ' has-error' : ''}}">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">R</div>
                                                                        <input type="number" class="form-control"
                                                                               name="price" autocomplete="off"
                                                                               title=""
                                                                               placeholder="Amount"
                                                                               value="{{old('price')}}">

                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="col-md-2">
                                                                <button type="submit"
                                                                        class="btn btn-md btn-dark btn-block waves-effect">
                                                                    <i
                                                                        class="fa fa-plus-circle"></i> Add Expense
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    @endpermission

                                                    <table id="expense_table"
                                                           class="table table-bordered table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th style="width: 10px">#</th>
                                                            <th>Item Name</th>
                                                            <th style="width: 100px;">Price</th>
                                                            @permission('delete-expenses')
                                                            <th style="width: 10px"></th>
                                                            @endpermission
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($expenses as $index => $expense)
                                                            <tr>
                                                                <td>{{ $index+1 }}</td>
                                                                <td>{{ $expense->item }}</td>
                                                                <td class="text-right">
                                                                    R {{number_format($expense->amount, 2)}}</td>
                                                                @permission('delete-expenses')
                                                                <td>
                                                                    <a href="{{ route('expenses.destroy', ['expense' => $expense]) }}"
                                                                       onclick="deleting('#delete-expense{{$expense->id}}', 'Income')"
                                                                       class="btn btn-xs btn-danger waves-effect"><i
                                                                            class="fa fa-trash"
                                                                            title="Delete expense"></i>
                                                                    </a>
                                                                    <form class="hidden"
                                                                          action="{{ route('expenses.destroy', ['expense' => $expense]) }}"
                                                                          method="POST"
                                                                          id="delete-expense{{$expense->id}}">
                                                                        {{ @csrf_field() }}
                                                                        {{ method_field("DELETE") }}
                                                                    </form>
                                                                </td>
                                                                @endpermission
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script src="{{asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/responsive.bootstrap.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('table').DataTable({
                stateSave: true,
                "ordering": true,
                language: {
                    searchPlaceholder: "Searching....",
                    decimal: ",",
                    thousands: "."
                },
                dom: 'frtp',
                responsive: true
            });
        });
    </script>
@endsection
