@extends('layouts.master')

@section('title')
    Event Finances
@endsection

@section('pageScript')
    <link href="{{asset('/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="page-content-wrapper ">
        <div class="container">
            @permission('add-events')
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group text-right">
                        <a href="{{route('events.index')}}" class="btn btn-md btn-dark waves-light waves-effect">
                            <i class="fa fa-plus-circle" title="Add Department"></i> Calendar
                        </a>
                    </div>
                </div>
            </div>
            @endpermission
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            @include('partials.error')
                            <table id="finance_table" class="table table-striped table-bordered"
                                   style="cursor: pointer;">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Event Date</th>
                                    <th>Incomes</th>
                                    <th>Expenses</th>
                                    @permission('edit-finances')
                                    <th style="width: 10px"></th>
                                    @endpermission
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($finances as $finance)
                                    <tr>
                                        <td>{{$finance->title}}</td>
                                        <td>{{ \Carbon\Carbon::parse($finance->start)->toDateString() }}</td>
                                        <td>{{$finance->incomes === null ? 'No incomes recorded' : 'R '. number_format($finance->incomes, 2)}}</td>
                                        <td>{{$finance->expenses === null ? 'No Expenses recorded' : 'R '. number_format($finance->expenses, 2)}}</td>
                                        @permission('edit-finances')
                                        <td>
                                            <a href="{{ route('finances.edit', ['finance' => $finance]) }}"
                                               class="btn btn-xs btn-block btn-success waves-effect">
                                                <i class="fa fa-pencil"></i> View </a>
                                        </td>
                                        @endpermission
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script src="{{asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/responsive.bootstrap.min.js')}}"></script>
    <script>
        $(document).ready(function () {

            $financetable = $('#finance_table');

            var table = $financetable.DataTable({
                stateSave: true,
                "ordering": false,
                language: {
                    searchPlaceholder: "Searching...."
                },
                dom: 'frtip',
                responsive: true
            });
        });
    </script>
@endsection
