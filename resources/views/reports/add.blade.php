@extends('layouts.master')

@section('title')
    Create Report
@endsection

@section('content')
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <h3 class="page-header text-center">Report Information</h3>
                            @include('partials.error')
                            @include('partials.formMessage')

                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <form role="form" method="POST" action="{{ route('reports.store') }}"
                                          enctype="multipart/form-data">

                                        {{csrf_field()}}

                                        <div class="col-lg-12">
                                            <div class="form-group{{$errors->has('title') ? ' has-error' : ' has-warning'}}">
                                                <label>Title</label>
                                                <input type="text" class="form-control" name="title" autocomplete="off"
                                                       title=""
                                                       placeholder="Title" value="{{old('title')}}">
                                            </div>
                                        </div>


                                        <div class="col-lg-6">
                                            <div
                                                class="form-group{{$errors->has('type_of_meeting') ? ' has-error' : ' has-warning'}}">
                                                <label for="type_of_meeting">Type of meeting</label>

                                                <select name="type_of_meeting" id="type_of_meeting" class="form-control">
                                                    <option value="">-- Select event type --</option>
                                                    @foreach($meetings as $meeting)
                                                        <option
                                                            value="{{ $meeting->id }}" {{old('type_of_meeting') == $meeting->id ? 'selected' : ''}}>
                                                            {{ $meeting->display_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>


                                        <div class="col-lg-6">
                                            <div
                                                class="form-group{{$errors->has('type_of_meeting') ? ' has-error' : ' has-warning'}}">
                                                <label for="department">Department</label>

                                                <select name="department" id="department" class="form-control">
                                                    <option value="">-- Select event type --</option>
                                                    @foreach($departments as $department)
                                                        <option
                                                            value="{{ $department->id }}" {{old('department') == $department->id ? 'selected' : ''}}>
                                                            {{ $department->display_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>

                                        <div class="col-lg-12">
                                            <div class="form-group{{$errors->has('file') ? ' has-error' : ' has-warning'}}">
                                                <label>Upload's Report:</label>
                                                <input id="upload" type="file" class="filestyle" name="file"
                                                       data-iconname="fa fa-cloud-upload" autocomplete="off"
                                                       value="{{old('file')}}">
                                            </div>
                                        </div>


                                        <div class="col-lg-12 pull-right">
                                            <div class="pull-right">
                                                @permission('add-reports')
                                                <button type="submit" class="btn btn-dark ">
                                                    <i class="fa fa-save"></i> Save
                                                </button>
                                                @endpermission
                                                <a href="{{ route('reports.index') }}" class="btn btn-default"><i
                                                        class="fa fa-reply"></i> Back</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript"
            src="{{asset('/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}"></script>
@endsection
