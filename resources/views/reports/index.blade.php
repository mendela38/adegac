@extends('layouts.master')

@section('pageScript')
    <link href="{{asset('/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/plugins/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('title')
    Reports
@endsection



@section('content')
    <div class="page-content-wrapper ">
        <div class="container">
            @permission('add-reports')
            <div class="form-group text-right">
                <a href="{{route('reports.create')}}" class="btn btn-md btn-dark waves-light waves-effect"><i
                            class="fa fa-plus-circle" title="Upload Report"></i> Upload Report
                </a>
            </div>
            @endpermission
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            @include('partials.error')

                            <table id="report" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Type of Meeting</th>
                                    <th>Date</th>
                                    <th>Department</th>
                                    @permission('read-reports|edit-reports|delete-reports')
                                    <th>Actions</th>
                                    @endpermission
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($reports as $report)
                                    <tr id="{{$report->id}}">
                                        <td>{{$report->title}}</td>
                                        <td>{{$report->meeting()->first()->display_name}}</td>

                                        <td>{{\Carbon\Carbon::parse($report->report_date)->format('M d, Y')}}</td>
                                        <td>{{ $report->role()->first()->display_name }}</td>

                                        @permission('read-reports|edit-reports|delete-reports')
                                        <td class="float-right">
                                            @permission('read-reports')
                                            @if($report->upload !== null)
                                            <a href="{{ route('reports.show', ['report' => $report]) }}" class="btn btn-xs btn-info waves-effect">
                                                <i class="fa fa-download" title="Download Report"></i>
                                            </a>
                                            @endif
                                            @endpermission
                                            @permission('edit-reports')
                                            <a href="{{ route('reports.edit', ['report' => $report]) }}" class="btn btn-xs btn-success waves-effect"><i
                                                        class="fa fa-pencil"></i>
                                            </a>
                                            @endpermission
                                            @permission('delete-reports')
                                            <a href="{{ route('reports.destroy', ['report' => $report]) }}" onclick="deleting('#delete-report{{$report->id}}', 'Bureau')"
                                               class="btn btn-xs btn-danger waves-effect"><i
                                                        class="fa fa-trash" title="Delete Member"></i>
                                            </a>
                                            <form class="hidden" action="{{ route('reports.destroy', ['report' => $report]) }}" method="POST" id="delete-report{{$report->id}}">
                                                {{ @csrf_field() }}
                                                {{ method_field("DELETE") }}
                                            </form>
                                            @endpermission
                                        </td>
                                        @endpermission
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('/plugins/datatables/responsive.bootstrap.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            $reportTable = $('#report');
             var table = indexPage($reportTable);
        });
    </script>
@endsection
