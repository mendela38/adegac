@extends('layouts.master')

@section('title')
    Edit Report
@endsection

@section('content')
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <h3 class="page-header text-center">Report Information</h3>
                            @include('partials.error')
                            @include('partials.formMessage')

                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <form role="form" method="POST" action="{{ route('reports.update', $report) }}"
                                          enctype="multipart/form-data">

                                        {{csrf_field()}}
                                        {{ method_field('PATCH') }}
                                        <input type="hidden" name="id" value="{{ $report->id }}">

                                        <div class="col-lg-12">
                                            <div
                                                class="form-group{{$errors->has('title') ? ' has-error' : ' has-warning'}}">
                                                <label>Title</label>
                                                <input type="text" class="form-control" name="title" autocomplete="off"
                                                       title=""
                                                       placeholder="Title"
                                                       value="{{old('title') ? old('title') : $report->title}}">
                                            </div>
                                        </div>


                                        <div class="col-lg-6">
                                            <div
                                                class="form-group{{$errors->has('type_of_meeting') ? ' has-error' : ' has-warning'}}">
                                                <label for="type_of_meeting">Type of meeting</label>

                                                <select name="type_of_meeting" id="type_of_meeting"
                                                        class="form-control">
                                                    <option value="">-- Select event type --</option>
                                                    @foreach($meetings as $meeting)
                                                        <option
                                                            value="{{ $meeting->id }}" {{old('type_of_meeting') === $meeting->id ? 'selected' : $report->meeting_id == $meeting->id ? 'selected' : ''}}>
                                                            {{ $meeting->display_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>


                                        <div class="col-lg-6">
                                            <div
                                                class="form-group{{$errors->has('type_of_meeting') ? ' has-error' : ' has-warning'}}">
                                                <label for="department">Department</label>

                                                <select name="department" id="department" class="form-control">
                                                    <option value="">-- Select event type --</option>
                                                    @foreach($departments as $department)
                                                        <option
                                                            value="{{ $department->id }}" {{old('department') === $department->id ? 'selected' : $report->role_id == $department->id ? 'selected' : ''}}>
                                                            {{ $department->display_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>

                                        <div class="col-lg-12">
                                            <div
                                                class="form-group{{$errors->has('file') ? ' has-error' : ' has-warning'}}">
                                                <label>Upload's
                                                    Report {{ $report->upload == null ? '- No file has been uploaded' : ' - Replace old file uploading a new one ' }}  @if($report->upload !== null)
                                                        <a href="{{ route('reports.show', ['report' => $report]) }}"
                                                           class="btn btn-xs btn-info waves-effect float-right">
                                                            <i class="fa fa-download" title="Download Report"></i>
                                                        </a>
                                                    @endif</label>
                                                <input id="upload" type="file" class="filestyle" name="file"
                                                       data-iconname="fa fa-cloud-upload" autocomplete="off"
                                                       readonly
                                                       value="{{ old('file') ? new \Illuminate\Http\UploadedFile(old('file'), old('file')) : $report->upload == null ? '' : new \Illuminate\Http\UploadedFile($report->upload, $report->upload) }}">
                                            </div>
                                        </div>


                                        <div class="col-lg-12">
                                            <div class="pull-right">

                                                @permission('edit-reports')
                                                <button type="submit" class="btn btn-dark ">
                                                    <i class="fa fa-save"></i> Save
                                                </button>
                                                @endpermission
                                                <a href="{{ route('reports.index') }}" class="btn btn-default"><i
                                                        class="fa fa-reply"></i> Back</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript"
            src="{{asset('/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}"></script>

@endsection
