@extends('layouts.master')

@section('pageScript')
    <link href="{{asset('/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
@endsection

@section('title')
    Create event
@endsection

@section('content')
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <h3 class="page-header text-center">Event Information</h3>
                            @include('partials.error')
                            @include('partials.formMessage')
                            <form method="POST" action="{{route('events.store')}}">

                                {{csrf_field()}}

                                {{--event name--}}
                                <div class="col-lg-12">
                                    {{--Event Name--}}
                                    <div class="form-group {{$errors->has('title') ? ' has-error' : 'has-warning'}}">
                                        <label for="">Event Name</label>
                                        <input type="text" class="form-control" name="title" title="" autocomplete="off"
                                               placeholder="Event Name" value="{{old('title')}}">
                                    </div>
                                    {{--event start date and time--}}
                                    <div class="row">
                                        <div class="col-lg-3 col-xs-12">
                                            <div class="form-group {{$errors->has('starting_date') ? $errors->has('starting_time') ? ' has-error' : 'has-warning': ' has-warning'}}">
                                                <label for="">Starting date and time</label>
                                                <div class="input-group col-lg-12">

                                                    <input type="date" class="form-control date"
                                                           name="starting_date"
                                                           placeholder="mm/dd/yyyy"
                                                           value="{{old('starting_date')}}" title="" autocomplete="off">
                                                    <span class="input-group-addon bg-custom b-0"><i
                                                                class="mdi mdi-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-xs-12">
                                            <div class="form-group {{$errors->has('starting_date') ? $errors->has('starting_time') ? ' has-error' : 'has-warning': ' has-warning'}}">
                                                <label for="">Starting date and time</label>
                                                <div class="input-group col-lg-12">
                                                    <input id="timepicker2" type="text" name="starting_time"
                                                           class="form-control timepicker" placeholder="hh:mm" title=""
                                                           autocomplete="off"
                                                           value="{{old('starting_time')}}">
                                                    <span class="input-group-addon"><i
                                                            class="fa fa-clock-o"></i></span>
                                                </div>
                                            </div>
                                        </div>

                                        {{--Department--}}
                                        <div class="col-lg-6 col-xs-12">
                                            <div class="form-group{{$errors->has('event_end') ? $errors->has('event_end') ? ' has-error': ' has-warning' : ' has-warning'}}">
                                                <label for="department">Department</label>

                                                <select name="department" id="department" class="form-control">
                                                    <option value="">-- Select a department</option>
                                                    @foreach($roles as $role)
                                                        <option value="{{ $role->id }}" {{ old('department') == $role->id ? 'selected' : '' }} >{{ $role->display_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>


                                    </div>

                                    {{--event location--}}
                                    <div class="form-group {{$errors->has('location') ? ' has-error' : ''}}">
                                        <label for="">Location (Optional)</label>

                                        <input type="text" class="form-control" name="location"
                                               placeholder="Event Location" title="" autocomplete="off"
                                               value="{{old('location')}}">
                                    </div>

                                    {{--event description--}}
                                    <div class="form-group">
                                        <label for="">Description (Optional)</label>

                                        <textarea class="form-control" name="description"
                                                  placeholder="Event Description"
                                                  rows="4">{{old('description')}}</textarea>
                                    </div>

                                    {{--create event button--}}
                                    <div class="form-group text-right">
                                        @permission('add-events')
                                        <button type="submit" class="btn btn-dark"><i class="fa fa-save"></i> Create</button>
                                        @endpermission
                                        @permission('browse-events')
                                        <a href="{{ route('events.index') }}" class="btn btn-default"><i class="fa fa-reply"></i> Back</a>
                                        @endpermission
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section ('script')
    <script src="{{asset('/plugins/timepicker/bootstrap-timepicker.js')}}"></script>
    <script src="{{asset('/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>

    <script>
        $(document).ready(function () {

            datePicker();

            timePicker();
        });
    </script>
@endsection
