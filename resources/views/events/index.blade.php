@extends('layouts.master')

@section('pageScript')
    <link href="{{asset('/plugins/fullcalendar/css/fullcalendar.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('/plugins/qtip/jquery.qtip.min.css')}}" rel="stylesheet">
@endsection

@section('title')
    Events
@endsection

@section('content')
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="form-group text-right">
                @permission('browse-finances')
                <a href="{{route('finances.index')}}" class="btn btn-md btn-dark waves-effect"><i
                            class="fa fa-calendar-check-o"></i> Events Finances</a>
                @endpermission
                @permission('add-events')
                <a href="{{route('events.create')}}" class="btn btn-md btn-dark waves-effect"><i
                            class="fa fa-calendar-plus-o"></i> Add Event</a>
                @endpermission
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            @include('partials.error')
                            <div id="calendar" class="col-lg-12"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section ('script')
    <script src="{{asset('/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
    <script src="{{asset('/plugins/moment/moment.js')}}"></script>
    <script src="{{asset('/plugins/fullcalendar/js/fullcalendar.min.js')}}"></script>
    <script src="{{asset('/plugins/qtip/jquery.qtip.min.js')}}"></script>
    <script src="{{asset('/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}"></script>
    <script>
        var BASEURL = "{{url('/')}}";
        !function (e) {
            "use strict";
            var t = function () {
            };
            t.prototype.init = function () {
                if (e.isFunction(e.fn.fullCalendar)) {

                    e("#calendar").fullCalendar({
                        header: {
                            left: "prev,next today",
                            center: "title",
                            right: "month,basicWeek,basicDay"
                        },
                        selectable: false,
                        selectHelper: false,
                        editable: false,
                        height: 500,
                        droppable: false,
                        duplicate: false,
                        navLinks: false,
                        draggable: false,
                        handleWindowResize: true,
                        eventLimit: true,
                        views: {
                            agenda: {
                                eventLimit: 3,
                                timeFormat: 'h:mm'
                            },
                            basic: {
                                eventLimit: 3,
                                timeFormat: 'h:mm'
                            },
                            month: {
                                eventLimit: 2,
                                timeFormat: 'h:mm'
                            },
                            week: {
                                eventLimit: 3,
                                timeFormat: 'h:mm'
                            },
                            day: {
                                eventLimit: 3,
                                timeFormat: 'h:mm'
                            }
                        },
                        events: [
                                @foreach($events as $event)
                            {
                                title: '{{ $event->title }}',
                                start: '{{ $event->date }}',
                                description: '{{ $event->description }}',
                                location: '{{ $event->location }}',
                                @permission('edit-events')
                                url: '{{ route('events.edit', $event) }}'
                                    @endpermission

                            },
                            @endforeach
                        ],
                        eventColor: '#378006',
                        eventRender: function (event, element, data) {

                            element.append(event.location);

                            element.qtip({
                                content: "<b>Title: </b>" + '<br />' + event.title + '<br /> <br />' +
                                    "<b>Description: </b>" + '<br />' + event.description + '<br />',
                                style: {
                                    background: 'black',
                                    color: '#FFFFFF'
                                },
                                position: {
                                    center: {
                                        target: 'center',
                                        tooltip: 'bottomMiddle'
                                    }
                                }
                            });
                        }
                    })
                } else alert("Event Model plugin is not installed")
            }, e.CalendarPage = new t, e.CalendarPage.Constructor = t
        }(window.jQuery), function (e) {
            "use strict";
            e.CalendarPage.init()
        }(window.jQuery);
    </script>
@endsection