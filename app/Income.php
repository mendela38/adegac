<?php

namespace App;

use App\Http\Requests\IncomeRequest;
use Illuminate\Database\Eloquent\Model;

class Income extends Model
{

    public static function createIncome(IncomeRequest $request)
    {
        // Create Income
        $income = New Income();
        $income->event_id = $request->event_id;
        $income->source = $request->source;
        $income->income_type = $request->type_of_income;
        $income->amount = $request->amount;
        $income->save();
        return $income;
    }

    public static function allIncomes($id)
    {
        return static::where('event_id', '=', $id)->get(['id', 'source', 'income_type', 'amount']);
    }
}
