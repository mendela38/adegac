<?php

namespace App;

use App\Http\Requests\RoleRequest;
use Illuminate\Support\Str;
use Laratrust\Models\LaratrustRole;

/**
 * @property mixed description
 * @property string display_name
 * @property mixed name
 * @property mixed user_id
 */
class Role extends LaratrustRole
{
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    //
    public static function createDepartment(RoleRequest $request)
    {
        return self::updateDepartment($request, new Role());
    }

    //
    public static function updateDepartment(RoleRequest $request, Role $role)
    {
        $role->display_name = $request->name;
        $role->name = Str::slug($request->name);
        $role->description  =$request->description;
        $role->user_id  =$request->active_user;
        $role->save();

        if ($role->permissions()->get()->count() != 0) {
            $role->permissions()->detach($request->permissions);
        }
        $role->permissions()->sync($request->permissions);
        return $role;
    }

    private function permissionToArray(){
        return $this->permissions()->get()->map(function($permission){
            return $permission->id;
        })->toArray();
    }

    public function rolePermission($permission, $array = null)
    {
        if ($array == null)
            $array = $this->permissionToArray();

        return in_array($permission, $array);
    }

    public function getRouteKeyName()
    {
        return 'name';
    }
}
