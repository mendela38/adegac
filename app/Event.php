<?php

namespace App;

use App\Http\Requests\EventRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Event extends Model
{
    //
    public static function createEvent(EventRequest $request)
    {
        return self::updateEvent($request, new Event());
    }

    public static function updateEvent(EventRequest $request, Event $event)
    {
        $event->title = $request->title;
        $event->date = $request->starting_date;
        $event->start = $request->starting_time;
        $event->role_id = $request->department;
        $event->location = $request->location;
        $event->description = $request->description;
        $event->save();
        return $event;
    }

    public static function financialSummary()
    {
        return static::select(DB::raw('events.id, title, start, totalIncomes.incomes, totalExp.expenses'))
            ->leftJoin(DB::raw('(SELECT incomes.event_id, SUM(amount) as incomes FROM `incomes` GROUP BY event_id) as totalIncomes'), function ($join) {
                $join->on('events.id', '=', 'totalIncomes.event_id');
            })
            ->leftJoin(DB::raw('(SELECT expenses.event_id, SUM(amount) as expenses FROM `expenses` GROUP BY event_id) totalExp'), function ($join) {
                $join->on('totalIncomes.event_id', '=', 'totalExp.event_id');
            })
//            ->leftJoin(DB::raw('(SELECT event_id, SUM(amount_oe) as otherExpenses  FROM `other_expenses` GROUP BY event_id) totalOexp'), function ($join) {
//                $join->on('totalExp.event_id', '=', 'totalOexp.event_id');
//            })
            ->get();
    }

}
