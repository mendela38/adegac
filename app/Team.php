<?php

namespace App;

use App\Http\Requests\TeamRequest;
use Illuminate\Support\Str;
use Laratrust\Models\LaratrustTeam;

/**
 * @property mixed name
 * @property string display_name
 * @property mixed end_date
 * @property mixed start_date
 * @property mixed description
 */
class Team extends LaratrustTeam
{
    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'role_user', 'team_id');
    }

    public static function createBureau(TeamRequest $request)
    {
        return static::updateBureau($request, new Team());
    }

    public static function updateBureau(TeamRequest $request, Team $team)
    {
        $team->display_name = $request->name;
        $team->name = Str::slug($request->name);
        $team->description = $request->description;
        $team->start_date = $request->start_date;
        $team->end_date = $request->end_date;
        $team->save();
        return $team;
    }

    public function getRouteKeyName()
    {
        return 'name';
    }
}
