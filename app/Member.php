<?php

namespace App;

use App\Http\Requests\MemberRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

/**
 * @property mixed last_name
 * @property mixed first_name
 * @property mixed dob
 * @property mixed pob
 * @property mixed gender
 * @property mixed passport_number
 * @property mixed occupation_title
 * @property mixed occupation_type
 * @property mixed occupation_place
 * @property mixed cell_number
 * @property mixed address
 * @property mixed email
 * @property mixed joining_date
 * @property false|string|null picture
 * @property mixed nk_last_name
 * @property mixed nk_first_name
 * @property mixed nk_contact
 * @property mixed state
 */
class Member extends Model
{
    /**
     * Members financial history relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function financialHistory()
    {
        return $this->hasMany(MemberFinance::class, 'member_id');
    }

    /**
     * Member User relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'member_id');
    }

    /**
     * Counting Member total per gender
     * @param null $gender
     * @return mixed
     */
    public static function countingMembers($gender = null)
    {
        if ($gender === null) {
            return static::count();
        } else {
            return static::where('gender', '=', $gender)->count();
        }
    }

    /**
     * Create New Member
     * @param MemberRequest $request
     * @return Member
     */
    public static function createMember(MemberRequest $request)
    {
        return self::updateMember($request, new Member());
    }

    /**
     * Update member
     * @param MemberRequest $request
     * @param Member $member
     * @return Member
     */
    public static function updateMember(MemberRequest $request, Member $member)
    {
        $member->last_name = $request->last_name;
        $member->first_name = $request->first_name;
        $member->dob = $request->date_of_birth;
        $member->pob = $request->place_of_birth;
        $member->gender = $request->gender;
        $member->passport_number = $request->passport_number;
        $member->occupation_title = $request->occupation_title;
        $member->occupation_type = $request->occupation_type;
        $member->occupation_place = $request->occupation_place;
        $member->cell_number = $request->cell_number;
        $member->address = $request->address;
        $member->email = $request->email;
        $member->joining_date = $request->joining_date;
        $member->nk_last_name = $request->nk_last_name;
        $member->nk_first_name = $request->nk_first_name;
        $member->nk_contact = $request->nk_contact;
        if (request()->hasFile('picture')) {
            $file = request()->file('picture');
            $filename = Str::slug($request->last_name.'-'.$request->first_name) . '.' . $file->extension();
            $path = $file->storeAs('public/users', $filename);

            // delete old path
            if ($member->picture !== null)
                File::delete(public_path($member->picture));

            $member->picture = '/storage/' . substr($path, 7);
        }
        $member->save();

        return $member;
    }

    /**
     * List members with teir contribution
     * @return mixed
     */
    public static function memberContributions()
    {
        //TODO return query of list of member contribution per year
        return static::select('members.id', 'last_name', 'first_name', 'passport_number', 'cell_number', 'email', 'finances.payment_date', 'finances.amount')
            ->leftJoin(DB::raw('(Select member_id, Max(payment_date) as payment_date, SUM(amount) as amount from member_finances Group by member_id) finances'), function ($join) {
                $join->on('members.id', '=', 'finances.member_id');
            })
            ->get();
    }

    /**
     * Route key name
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'passport_number';
    }
}
