<?php

namespace App;

use App\Http\Requests\ExpenseRequest;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed item
 * @property mixed amount
 * @property mixed event_id
 */
class Expense extends Model
{
    //
    public static function allExpenses($id)
    {
        return static::where('event_id', '=', $id)->get(['id', 'item', 'amount']);
    }

    public function createExpense(ExpenseRequest $request)
    {
        $this->event_id = $request->event_id;
        $this->item = $request->item;
        $this->amount = $request->price;
        $this->save();
        return $this;
    }
}
