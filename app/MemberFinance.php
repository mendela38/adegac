<?php

namespace App;

use App\Http\Requests\ContributionRequest;
use Illuminate\Database\Eloquent\Model;

class MemberFinance extends Model
{
    public $timestamps = false;
    //
    public function memberPayment(ContributionRequest $request)
    {
        $memberFinance = new MemberFinance();
        $memberFinance->member_id = $request->member_id;
        $memberFinance->payment_date = $request->payment_date;
        $memberFinance->amount = $request->amount;
        $memberFinance->save();
    }
}
