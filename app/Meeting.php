<?php

namespace App;

use App\Http\Requests\MeetingRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * @property mixed display_name
 * @property string name
 * @property mixed description
 */
class Meeting extends Model
{
    //
    public static function createMeeting(MeetingRequest $request)
    {
        return self::updateMeeting($request, new Meeting());
    }

    public static function updateMeeting(MeetingRequest $request, Meeting $meeting)
    {
        $meeting->display_name = $request->name;
        $meeting->name = Str::slug($request->name);
        $meeting->description = $request->description;
        $meeting->save();
        return $meeting;
    }
}
