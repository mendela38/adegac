<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Redirect to people index page
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index()
    {
        return redirect()->route('people.index');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->back();
    }
}
