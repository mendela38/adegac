<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContributionRequest;
use App\Member;
use App\MemberFinance;

class ContributionController extends Controller
{
    /**
     * MemberController constructor.
     */
    public function __construct()
    {
        $this->middleware(['verified']);
        $this->middleware('permission:browse-contributions')->only(['index']);
        $this->middleware('permission:add-contributions')->only(['create', 'store']);
        $this->middleware('permission:read-contributions')->only(['show']);
        $this->middleware('permission:edit-contributions')->only(['edit', 'update']);
        $this->middleware('permission:delete-contributions')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Member::memberContributions();
        return view('contributions.index')->withMembers($members);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ContributionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContributionRequest $request)
    {
        (new MemberFinance())->memberPayment($request);
        return redirect()->back()->withSuccess("Added new contribution !!");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Member $contribution
     * @return \Illuminate\Http\Response
     */
    public function edit(Member $contribution)
    {
        return view('contributions.edit')->withMember($contribution);
    }

    public function destroy(MemberFinance $contribution)
    {
        $contribution->delete();
        return redirect()->back()->withSuccess("Contribution deleted!!");
    }
}
