<?php

namespace App\Http\Controllers;

use App\Http\Requests\IncomeRequest;
use App\Income;
use Illuminate\Http\Request;

class IncomeController extends Controller
{
    /**
     * MemberController constructor.
     */
    public function __construct()
    {
        $this->middleware(['verified']);
        $this->middleware('permission:browse-incomes')->only(['index']);
        $this->middleware('permission:add-incomes')->only(['create', 'store']);
        $this->middleware('permission:read-incomes')->only(['show']);
        $this->middleware('permission:edit-incomes')->only(['edit', 'update']);
        $this->middleware('permission:delete-incomes')->only(['destroy']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param IncomeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(IncomeRequest $request)
    {
        $income = Income::createIncome($request);
        return redirect()->back()->withSuccess('New Income added!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Income $income
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Income $income)
    {
        $income->delete();
        return redirect()->back()->withSuccess("Income deleted!!");
    }
}
