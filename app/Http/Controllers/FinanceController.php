<?php

namespace App\Http\Controllers;

use App\Event;
use App\Expense;
use App\Income;
use Illuminate\Http\Request;

class FinanceController extends Controller
{
    /**
     * MemberController constructor.
     */
    public function __construct()
    {
        $this->middleware(['verified']);
        $this->middleware('permission:browse-finances')->only(['index']);
        $this->middleware('permission:add-finances')->only(['create', 'store']);
        $this->middleware('permission:read-finances')->only(['show']);
        $this->middleware('permission:edit-finances')->only(['edit', 'update']);
        $this->middleware('permission:delete-finances')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('finance.index')->withFinances(Event::financialSummary());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $finance)
    {
        $incomes = Income::allIncomes($finance->id);
        $expenses = Expense::allExpenses($finance->id);
        return view("finance.edit")->withIncomes($incomes)->withExpenses($expenses)->withEvent($finance);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
