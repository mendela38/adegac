<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReportRequest;
use App\Meeting;
use App\Report;
use App\Role;
use Illuminate\Support\Facades\File;

class ReportController extends Controller
{
    /**
     * MemberController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:browse-reports')->only(['index']);
        $this->middleware('permission:add-reports')->only(['create', 'store']);
        $this->middleware('permission:read-reports')->only(['show']);
        $this->middleware('permission:edit-reports')->only(['edit', 'update']);
        $this->middleware('permission:delete-reports')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('reports.index')->withReports(Report::latest()->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reports.add')->withMeetings(Meeting::all())->withDepartments(Role::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ReportRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReportRequest $request)
    {

        $report = Report::createReport($request);
        return redirect()->route('reports.index')->withSuccess("New Reprot '$report->title' has been uploaded!!");
    }

    /**
     * Display the specified resource.
     *
     * @param Report $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        return response()->download(public_path($report->upload));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Report $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        return view('reports.edit')->withReport($report)->withMeetings(Meeting::all())->withDepartments(Role::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ReportRequest $request
     * @param Report $report
     * @return \Illuminate\Http\Response
     */
    public function update(ReportRequest $request, Report $report)
    {
        $report = Report::updateReport($request, $report);
        return redirect()->route('reports.index')->withSuccess("Reprot '$report->title' has been updated!!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Report $report
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Report $report)
    {
        if (File::delete(public_path($report->upload))) {
            $report->delete();
            return redirect()->route('reports.index')->withSuccess("Report '$report->title' has been updated!!");
        };
        return redirect()->back()->withMessage("Fail to delete Report.");
    }
}
