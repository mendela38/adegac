<?php

namespace App\Http\Controllers;

use App\Http\Requests\TeamRequest;
use App\Permission;
use App\Team;
use App\User;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * MemberController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:browse-bureaus')->only(['index']);
        $this->middleware('permission:add-bureaus')->only(['create', 'store']);
        $this->middleware('permission:read-bureaus')->only(['show']);
        $this->middleware('permission:edit-bureaus')->only(['edit', 'update']);
        $this->middleware('permission:delete-bureaus')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bureaus = Team::latest('id')->get();
        return view('bureau.index')->withBureaus($bureaus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bureau.add')->withMembers(User::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TeamRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamRequest $request)
    {
        $bureau = Team::createBureau($request);
        return redirect()->route('bureau.index')->withSuccess("New Bureau '$bureau->name' has been created!!");
    }

    /**
     * Display the specified resource.
     *
     * @param Team $bureau
     * @return \Illuminate\Http\Response
     */
    public function show(Team $bureau)
    {
        return view('bureau.view')->withBureau($bureau);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Team $bureau
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $bureau)
    {
        return view('bureau.edit')->withBureau($bureau)->withMembers(User::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TeamRequest $request
     * @param Team $bureau
     * @return \Illuminate\Http\Response
     */
    public function update(TeamRequest $request, Team $bureau)
    {
        $bureau = Team::updateBureau($request, $bureau);
        if ($bureau->users()->count() !== 0)
            return redirect()->route('bureau.show', ['bureau' => $bureau])->withSuccess("New Bureau '$bureau->display_name' has been created!!");
        return redirect()->route('bureau.index')->withSuccess("New Bureau '$bureau' has been created!!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Team $bureau
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Team $bureau)
    {
        $bureau->delete();
        return redirect()->back()->withSuccess("Bureau '$bureau->display_name' has been deleted");
    }
}
