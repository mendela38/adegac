<?php

namespace App\Http\Controllers;

use App\Http\Requests\MeetingRequest;
use App\Meeting;

class MeetingController extends Controller
{
    /**
     * MemberController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:browse-meetings')->only(['index']);
        $this->middleware('permission:add-meetings')->only(['create', 'store']);
        $this->middleware('permission:read-meetings')->only(['show']);
        $this->middleware('permission:edit-meetings')->only(['edit', 'update']);
        $this->middleware('permission:delete-meetings')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meetings = Meeting::latest('id')->get();
        return view('meeting.index')->withMeetings($meetings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('meeting.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param MeetingRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(MeetingRequest $request)
    {
        $meeting = Meeting::createMeeting($request);
        return redirect()->route('meetings.index')->withSuccess("New Meeting '$meeting->display_name' has been created!!");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Meeting $meeting
     * @return \Illuminate\Http\Response
     */
    public function edit(Meeting $meeting)
    {
        return view('meeting.edit')->withMeeting($meeting);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MeetingRequest $request
     * @param Meeting $meeting
     * @return \Illuminate\Http\Response
     */
    public function update(MeetingRequest $request, Meeting $meeting)
    {
        $meeting = Meeting::updateMeeting($request, $meeting);
        return redirect()->route('meetings.index')->withSuccess("Meeting '$meeting->display_name' has been updated!!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Meeting $meeting
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Meeting $meeting)
    {
        $meeting->delete();
        return redirect()->back()->withSuccess("Meeting '$meeting->name' has been deleted");
    }
}
