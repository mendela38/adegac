<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Member;
use App\Role;
use App\Team;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * MemberController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:browse-users')->only(['index']);
        $this->middleware('permission:add-users')->only(['create', 'store']);
        $this->middleware('permission:read-users')->only(['show']);
        $this->middleware('permission:edit-users')->only(['edit', 'update']);
        $this->middleware('permission:delete-users')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::latest('id')->get();
        // ToDO Make Query that get the list of available users
        return view('user.index')->withUsers($users)->withMembers(Member::all())->withBureaus(Team::all())->withDepartments(Role::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.add')->withMembers(Member::all())
            ->withBureaus(Team::all())->withDepartments(Role::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user = User::createUser($request);
        return redirect()->route('users.index')->withSuccess("New User '".$user->email."' has been assigned to a member !!");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('user.edit')->withUser($user)->withMembers(Member::all())
            ->withBureaus(Team::all())->withDepartments(Role::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserRequest $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $user = User::updateUser($request, $user);
        return redirect()->route('users.index')
            ->withSuccess("User '".$user->email."' has been assigned to a member!!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->back()->withSuccess("User has been deleted!!");
    }
}
