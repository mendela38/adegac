<?php

namespace App\Http\Controllers;

use App\Http\Requests\MemberRequest;
use App\Member;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\DeclareDeclare;

class MemberController extends Controller
{
    /**
     * MemberController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:browse-people')->only(['index']);
        $this->middleware('permission:add-people')->only(['create', 'store']);
        $this->middleware('permission:read-people')->only(['show']);
        $this->middleware('permission:edit-people')->only(['edit', 'update']);
        $this->middleware('permission:delete-people')->only(['destroy']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Member::latest('id')->get();
        return view('people.index')->withPeople($members);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('people.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param MemberRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(MemberRequest $request)
    {
        $member = Member::createMember($request);
        return redirect()->route('people.index')->withSuccess("New Member '$member->last_name' has been added!");
    }

    /**
     * Display the specified resource.
     *
     * @param Member $person
     * @return \Illuminate\Http\Response
     */
    public function show(Member $person)
    {
        return view('people.view')->withPerson($person);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Member $person
     * @return
     */
    public function edit(Member $person)
    {
        return view('people.edit')->withPerson($person);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MemberRequest $request
     * @param Member $person
     * @return \Illuminate\Http\Response
     */
    public function update(MemberRequest $request, Member $person)
    {
        Member::updateMember($request, $person);
        return redirect()->route('people.show', $person)->withSuccess("Member updated.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Member $person
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Member $person)
    {
        $person->delete();
        return redirect()->route('people.index')->withSuccess("Member deleted");
    }
}
