<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleRequest;
use App\Permission;
use App\Role;
use App\User;

class RoleController extends Controller
{
    private $permissionCategories = ['people','bureaus','departments','reports','users', 'events', 'finances','contributions','incomes','expenses','profile'];
    /**
     * MemberController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:browse-departments')->only(['index']);
        $this->middleware('permission:add-departments')->only(['create', 'store']);
        $this->middleware('permission:read-departments')->only(['show']);
        $this->middleware('permission:edit-departments')->only(['edit', 'update']);
        $this->middleware('permission:delete-departments')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Role::latest('id')->get();
        return view('department.index')->withDepartments($departments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Retrieve Permission details

        return view('department.add')->withPermissions(Permission::all())->withPermissionCategories($this->permissionCategories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoleRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $department = Role::createDepartment($request);
        return redirect()->route('departments.index')->withSuccess("New Department '$department->display_name' has been created!!");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Role $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $department)
    {
        return view('department.edit')->withDepartment($department)->withPermissions(Permission::all())->withPermissionCategories($this->permissionCategories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoleRequest $request
     * @param Role $department
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, Role $department)
    {
        $department = Role::updateDepartment($request, $department);
        return redirect()->route('departments.index')->withSuccess("Department '$department->display_name' has been updated!!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Role $department
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Role $department)
    {
        $department->delete();
        return redirect()->back()->withSuccess("Department '$department->name' has been deleted");
    }
}
