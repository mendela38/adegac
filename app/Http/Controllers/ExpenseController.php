<?php

namespace App\Http\Controllers;

use App\Expense;
use App\Http\Requests\ExpenseRequest;

class ExpenseController extends Controller
{
    /**
     * MemberController constructor.
     */
    public function __construct()
    {
        $this->middleware(['verified']);
        $this->middleware('permission:browse-expenses')->only(['index']);
        $this->middleware('permission:add-expenses')->only(['create', 'store']);
        $this->middleware('permission:read-expenses')->only(['show']);
        $this->middleware('permission:edit-expenses')->only(['edit', 'update']);
        $this->middleware('permission:delete-expenses')->only(['destroy']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ExpenseRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExpenseRequest $request)
    {
        (new Expense())->createExpense($request);
        return redirect()->back()->withSuccess('New Expense added!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Expense $expense
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Expense $expense)
    {
        $expense->delete();
        return redirect()->back()->withSuccess("Expense deleted!!");
    }
}
