<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Requests\EventRequest;
use App\Role;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * MemberController constructor.
     */
    public function __construct()
    {
        $this->middleware(['verified']);
        $this->middleware('permission:browse-events')->only(['index']);
        $this->middleware('permission:add-events')->only(['create', 'store']);
        $this->middleware('permission:read-events')->only(['show']);
        $this->middleware('permission:edit-events')->only(['edit', 'update']);
        $this->middleware('permission:delete-events')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('events.index')->withEvents(Event::all());
    }

    public function eventList()
    {
        // Load calendar with events
        return Response()->json(Event::all('id', 'title', 'date', 'start', 'description', 'location'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('events.add')->withRoles(Role::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventRequest $request)
    {
        $event = Event::createEvent($request);
        return redirect()->route('events.index')->withSuccess("New Project '$event->title' has been added!!");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Event $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        return view('events.edit')->withEvent($event)->withRoles(Role::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EventRequest $request
     * @param Event $event
     * @return \Illuminate\Http\Response
     */
    public function update(EventRequest $request, Event $event)
    {
        $event = Event::updateEvent($request, $event);
        return redirect()->route('events.index')->withSuccess("Project '$event->title' has been Updated!!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Event $event
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Event $event)
    {
        $event->delete();
        return redirect()->route('events.index')->withSuccess("Project '$event->title' has been deleted!!");
    }
}
