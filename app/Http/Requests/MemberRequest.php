<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed last_name
 * @property mixed first_name
 * @property mixed nk_contact
 * @property mixed nk_first_name
 * @property mixed nk_last_name
 * @property mixed joining_date
 * @property mixed email
 * @property mixed address
 * @property mixed cell_number
 * @property mixed occupation_place
 * @property mixed occupation_type
 * @property mixed occupation_title
 * @property mixed passport_number
 * @property mixed gender
 * @property mixed place_of_birth
 * @property mixed date_of_birth
 */
class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'picture' => 'sometimes|image|mimes:jpg,jpeg,png',
            'last_name' => 'required|min:3',
            'first_name' => 'required|min:3',
            'date_of_birth' => 'required|date',
            'place_of_birth' => '',
            'gender' => 'required',
            'passport_number' => 'required|alpha_num|min:9|max:11',
            'occupation_title' => 'required',
            'occupation_type' => '',
            'occupation_place' => '',
            'cell_number' => 'required|max:10',
            'address' => 'sometimes|',
            'email' => 'required|email',
            'joining_date' => 'sometimes|',
            'nk_last_name' => 'required|min:3',
            'nk_first_name' => 'required|min:3',
            'nk_contact' => 'required|'
        ];
    }
}
