<?php

namespace App\Http\Requests;

use App\Report;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed title
 * @property mixed type_of_meeting
 * @property mixed department
 * @property mixed id
 */
class ReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id !== null ? ','.$this->id : '';
//        dd($this);
        return [
            'title' => 'required|unique:reports,id'.$id,
            'type_of_meeting' => 'required|',
            'department' => 'required|',
            'file' => 'sometimes|max:2000|mimes:jpeg,bmp,png,jpg,pdf,docx,doc',
        ];
    }
}
