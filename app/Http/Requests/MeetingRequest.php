<?php

namespace App\Http\Requests;

use App\Meeting;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed name
 * @property mixed description
 */
class MeetingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Meeting::whereName($this->id)->first() === null ? '' : ','.Meeting::whereName($this->id)->first()->id;
        return [
            'name' => 'required|string|unique:meetings,id'.$id,
            'description' => 'sometimes|string|max:200'
        ];
    }
}
