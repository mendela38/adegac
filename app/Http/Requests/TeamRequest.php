<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed name
 * @property mixed start_date
 * @property mixed end_date
 * @property mixed description
 */
class TeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'sometimes|',
            'start_date' => 'required|date',
            'end_time' => 'sometimes|date',
            'members' => 'sometimes|'
        ];
    }
}
