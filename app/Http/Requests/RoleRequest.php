<?php

namespace App\Http\Requests;

use App\Role;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed name
 * @property mixed description
 * @property mixed permissions
 */
class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request('department') === null ? '' : ','.request('department')->id;
        return [
            'name' => 'required',
            'description' => 'sometimes|',
            'active_user' => "sometimes|nullable|unique:roles,user_id".$id,
            'permissions' => 'required|'
        ];
    }
}
