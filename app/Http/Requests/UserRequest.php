<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed member
 * @property mixed password
 * @property mixed bureau
 * @property mixed department
 * @property mixed state
 */
class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'member' => ['required','nullable'],
            'bureau' => ['required'],
            'department' => ['required'],
            'password' => ['sometimes', 'string', 'min:6', 'confirmed'],
            'state' => ['sometimes']
        ];
    }
}
