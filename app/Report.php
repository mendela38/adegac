<?php

namespace App;

use App\Http\Requests\ReportRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

/**
 * @property mixed upload
 * @property mixed title
 * @property mixed meeting_id
 * @property string name
 * @property mixed role_id
 */
class Report extends Model
{
    public function meeting()
    {
        return $this->belongsTo(Meeting::class, 'meeting_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    //
    public static function updateReport(ReportRequest $request, Report $report)
    {
        $path = null;
        if (request()->hasFile('file')) {
            $file = request()->file('file');
            $filename = Str::slug($request->title) . '.' . $file->extension();
            $path = $file->storeAs('public/reports', $filename);

            // delete old path
            if ($report->upload !== null)
                File::delete(public_path($report->upload));
        }
        $report->title = $request->title;
        $report->name = Str::slug($request->title);
        $report->meeting_id = $request->type_of_meeting;
        $report->role_id = $request->department;
        if (request()->hasFile('file')) {
            $report->upload = 'storage/' . substr($path, 7);
        }
        $report->save();
        return $report;
    }

    public static function createReport(ReportRequest $request)
    {
        return self::updateReport($request, new Report());
    }

    public function getRouteKeyName()
    {
        return 'name';
    }
}
