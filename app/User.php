<?php

namespace App;

use App\Http\Requests\UserRequest;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;

/**
 * @property mixed member_id
 * @property mixed password
 * @property mixed email
 * @property mixed state
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get Department id
     * @return string
     */
    public function department()
    {
        return $this->roles()->first() !== null ? $this->roles()->first()->id : '';
    }

    /**
     * Get Bureau Id
     * @return string
     */
    public function bureau()
    {
        return $this->rolesTeams()->first() !== null ? $this->rolesTeams()->first()->id : '';
    }

    /**
     * Create a user
     * @param UserRequest $request request validation
     * @return User
     */
    public static function createUser(UserRequest $request)
    {
        return User::updateUser($request, new User());
    }

    /**
     * Update a user
     * @param UserRequest $request
     * @param User $user
     * @return User
     */
    public static function updateUser(UserRequest $request, User $user)
    {
        if (!isset($user->password) && $request->password == null)
            $user->password = bcrypt('123456');
        else if (isset($user->password) && $request->password !== null)
            $user->password = bcrypt($request->password);
        else if ($request->password !== null)
            $user->password = bcrypt($request->password);

        $user->email = Member::whereId($request->member)->first()->email;
        if ($request->state == null)
            $user->state = 1;
        else
            $user->state = $request->state;
        $user->save();

        $user->member()->attach($request->member);
        $bureau = Team::whereId($request->bureau)->first();
        $department = Role::whereId($request->department)->first();


        if ($user->hasRole($department, $bureau))
            $user->detachRole($department, $bureau);
        $user->attachRole($department, $bureau);
        return $user;
    }

    public function fullName($value = null)
    {
        return $this->member()->first() == null ? $value == null ? $this->email : $value : $this->member()->first()->first_name . ' ' . $this->member()->first()->last_name;
    }

    public function member()
    {
        return $this->belongsToMany(Member::class, 'member_user', 'user_id','member_id');
    }

    public static function unassignedUsers()
    {
        return User::join('roles', 'users.id', '<>', 'roles.user_id')->get();
    }
}
