<?php

return [
    'role_structure' => [
        'superadministrator' => [
            'people' => 'b,r,e,a,d',
            'bureaus' => 'b,r,e,a,d',
            'departments' => 'b,e,a,d',
            'reports' => 'b,r,e,a,d',
            'meetings' => 'b,e,a,d',
            'users' => 'b,r,e,a,d',
            'events' => 'b,e,a,d',
            'finances' => 'b,e',
            'contributions' => 'b,e,a,d',
            'incomes' => 'a,d',
            'expenses' => 'a,d',
            'profile' => 'r,e'
        ],
        'president' => [
            'people' => 'b,r,e,a,d',
            'profile' => 'r,e'
        ],
        'secretaire_general' => [
            'profile' => 'r,e'
        ],
        'secretaire_general_adjoint' => [
            'profile' => 'r,e'
        ],
        'commissaire_aux_comptes' => [
            'profile' => 'r,e'
        ],
        'affaires_culturelles' => [
            'profile' => 'r,e'
        ],
        'affaires_culturelles_adjoint' => [
            'profile' => 'r,e'
        ],
        'affaires_sociales' => [
            'profile' => 'r,e'
        ],
        'affaires_sociales_adjoint' => [
            'profile' => 'r,e'
        ],
        'orientations_academiques_professionnelles' => [
            'profile' => 'r,e'
        ],
        'orientations_academiques_professionnelles_Adjoint' => [
            'profile' => 'r,e'
        ],
        'communication' => [
            'profile' => 'r,e'
        ],
        'communication_adjoint' => [
            'profile' => 'r,e'
        ],
    ],
    'permissions_map' => [
        'b' => 'browse',
        'r' => 'read',
        'e' => 'edit',
        'a' => 'add',
        'd' => 'delete'
    ]
];
