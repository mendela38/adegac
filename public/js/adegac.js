function deleting($value, $section) {
    event.preventDefault();
    swal({
        title: "Are you sure?",
        text: $section + " record will be deleted",
        type: "error",
        showCancelButton: !0,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: !0,
        closeOnCancel: !0
    }, function (e) {
        if (e) {
            $($value).submit()
        }
    });
}

function datePicker() {
    let date = new Date();
    let today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('.date').datepicker({
        autoclose: !0,
        format: "yyyy-mm-dd",
        todayHighlight: !0,
        clearBtn: !0,
        startView: 'decade',
        minView: 'decade',
        viewSelect: 'decade',
    }).datepicker( 'setDate', today );
}

function timePicker() {
    $('.timepicker').timepicker({
        defaultTIme: !1,
        showMeridian: !1,
        autoclose: !0,
    });
}

function indexPage(tableQuery) {
    $table = tableQuery.DataTable({
        stateSave: true,
        "ordering": true,
        language: {
            searchPlaceholder: "Searching...."
        },
        dom: 'frtp',
        responsive: true
    });
    return $table;
}

