<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes(['verify' => true]);
Route::get('logout', 'HomeController@logout')->name('logout');

Route::group(['middleware' => 'verified'], function () {
    Route::get('/', 'HomeController@index');
    Route::resource('people', 'MemberController');
    Route::resource('users', 'UserController');
    Route::resource('bureau', 'TeamController');
    Route::resource('departments', 'RoleController')->except(['show']);
    Route::resource('reports', 'ReportController');
    Route::resource('meetings', 'MeetingController')->except(['show']);
    Route::resource('events', 'EventController')->except(['show']);
    Route::resource('finances', 'FinanceController')->only(['index', 'edit']);
    Route::resource('contributions', 'ContributionController')->only(['index', 'edit', 'store', 'destroy']);
    Route::resource('incomes', 'IncomeController')->only(['store', 'destroy']);
    Route::resource('expenses', 'ExpenseController')->only(['store', 'destroy']);
});
